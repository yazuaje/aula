<?php
  # Iniciando la variable de control que permitirá mostrar o no el modal
  $exibirModal = false;
  # Verificando si existe o no la cookie
  if(!isset($_COOKIE["mostrarModal"]))
  {
    # Caso no exista la cookie entra aquí
    # Creamos la cookie con la duración que queramos
     
    //$expirar = 3600; // muestra cada 1 hora
    //$expirar = 10800; // muestra cada 3 horas
    //$expirar = 21600; //muestra cada 6 horas
    $expirar = 43200; //muestra cada 12 horas
    //$expirar = 86400;  // muestra cada 24 horas
    setcookie('mostrarModal', 'SI', (time() + $expirar)); // mostrará cada 12 horas.
    # Ahora nuestra variable de control pasará a tener el valor TRUE (Verdadero)
    $exibirModal = true;
  }
?>
<style>
.nombre_pop {
    font-size: 24px;
    text-align: center;
    padding-bottom: 16px;
}

.verificar_infor {
    background-color: blue;
    color: white;
    padding: 10px 20px 10px 20px;
    float: left;
    clear: both;
}

.cabezera_1 {
    background-color: brown;
    height: 99px;
    margin-bottom: 20px;
}
</style>


<link href="jquery.simple-popup.min.css" rel="stylesheet" type="text/css">
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Mostrar Ventana Emergente</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap.min.css">
  <script src="jquery.min.js"></script>
  <script src="bootstrap.min.js"></script>
</head>
<body>



<div class="container">

<a class="demo-2 btn btn-primary">HTML Block</a>

            <div id="popup1">
               asas
            </div>
</div>
























  <div class="container">
  <h2>Ejemplo</h2>
    <!-- Modal -->
    <div class="modal fade" id="modalInicio" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
       <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="cabezera_1">
            <div class="logo_nuevo_h"></div>
            </div>
            
            
            <div class="centro_pop">
            <div class="nombre_pop">Bienvenido(a) xxxx</div>
                        Le damos la bienvenida a nuestra institución y a su aula virtual, en ella podrá acceder a las presentaciones de las ponencias, ver su cronograma de capacitación, verificar su registró de asistencia, calificación de sus trabajos, descarga de libros virtuales, audio libros, informes técnicos y tutoriales especializados. 

Le Recomendamos verificar sud Datos Personales:


<div class="verificar_infor">Verificar Datos Personales</div>                                      
            </div>
         
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php if($exibirModal === true) : // Si nuestra variable de control "$exibirModal" es igual a TRUE activa nuestro modal y será visible a nuestro usuario. ?>
  <script>
  $(document).ready(function()
  {
    // id de nuestro modal
    $("#modalInicio").modal("show");
  });
  </script>
  <?php endif; ?>
<script src="jquery.simple-popup.min.js"></script>
<script>
$(document).ready(function() {
  $("a.demo-1").simplePopup();
  $("a.demo-2").simplePopup({ type: "html", htmlSelector: "#popup1" });
});
</script>
</body>
</html>