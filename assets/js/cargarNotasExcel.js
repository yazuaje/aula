
var X = XLSX;
var XW = {
	msg: 'xlsx',
	worker: 'http://72.29.67.11/~bvahost1/aula/assets/js/xlsxworker.js'
};

var global_wb;
var data;
var process_wb = (function() {
	var to_json = function to_json(workbook) {
		var result = {};
		workbook.SheetNames.forEach(function(sheetName) {
			var roa = X.utils.sheet_to_json(workbook.Sheets[sheetName], {header:1});
			if(roa.length) result[sheetName] = roa;
		});
		return JSON.stringify(result, 2, 2);
	};
	return function process_wb(wb) {
		global_wb = wb;
		window.data = JSON.parse(to_json(wb));		
		$.each(data,function(n,hoja){
			$.each(hoja,function(i,col){

				if(i>0)
				{
					var dni = parseInt(col[0])
					var fila = $('#'+dni)
					if(fila.length>0)
					{
						var modulos = fila.find('.modulo')
						if(modulos.length>0)
						{
							var acumulado = 0;
							var nModulos = 0;
							var promedio = 0;
							for (var p = 0; p <= col.length-1; p++) {
								if(p>1 && col[p]!="" && col[p])
								{
									
									var nota = parseFloat(col[p].replace(',','.'));

									if(isNaN(nota)) nota = 0;

									modulos.eq(nModulos).find('input').val(nota);
									modulos.eq(nModulos).find('div').html(nota);
									acumulado+=nota;
									nModulos++;
									
								}
								
								
							}
							if(modulos.length>0)
							{
								promedio = Math.round(acumulado/modulos.length);

							}
							fila.find('.promedio').html(promedio);
						}
					}
				}
				

			})
		})
	};
})();





var do_file = (function() {
	var rABS = typeof FileReader !== "undefined" && (FileReader.prototype||{}).readAsBinaryString;
	var domrabs = document.getElementsByName("userabs")[0];
	//if(!rABS) domrabs.disabled = !(domrabs.checked = false);

	var use_worker = typeof Worker !== 'undefined';
	var domwork = document.getElementsByName("useworker")[0];
//	if(!use_worker) domwork.disabled = !(domwork.checked = false);

	var xw = function xw(data, cb) {
		var worker = new Worker(XW.worker);
		worker.onmessage = function(e) {
			switch(e.data.t) {
				case 'ready': break;
				case 'e': console.error(e.data.d); break;
				case XW.msg: cb(JSON.parse(e.data.d)); break;
			}
		};
		worker.postMessage({d:data,b:rABS?'binary':'array'});
	};

	return function do_file(files) {
		rABS = true;
		use_worker = true;
		var f = files[0];
		var reader = new FileReader();
		reader.onload = function(e) {
			
			var data = e.target.result;
			
			xw(data, process_wb);
			
		};
		if(rABS) reader.readAsBinaryString(f);
		else reader.readAsArrayBuffer(f);
	};
})();

(function() {
	var xlf = document.getElementById('xlf');
	if(!xlf.addEventListener) return;
	function handleFile(e) { do_file(e.target.files); }
	xlf.addEventListener('change', handleFile, false);
})();