<?php

require_once ('tcpdf.php');

class Pdf extends TCPDF
{
    function __construct()
    {
        parent::__construct();
       
    }
}
class MYPDF extends TCPDF {

	//Page header
	public function Header() {
		// Logo

		$image_file = './imgs/-Cintillo-ZAMORA-2017-03.png';
		$this->Image($image_file, 5, 5, 170, 30, 'png', '', 'T', FALSE, 400, 'L', false, false, 0, true, false, false);
        //$image_file = K_PATH_IMAGES.'gobierno_encabezado.jpg';
        //$this->Image($image_file, 10, 10, 210, 30, 'JPG', '', 'T', false, 500, 'C', false, false, 0, true, false, false);
		// Set font
        //$this->writeHTML($html, true, false, true, false, '');
		$this->SetFont('helvetica', 'B', 15);
		// Title
		//$this->Cell(0, 15, '<< TCPDF Example 003 >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');
	}

	// Page footer
	public function Footer() {
		// Position at 15 mm from bottom
		$this->SetY(-15);
		// Set font
		$this->SetFont('helvetica', '', 10);
        /*$html='
        <p align="right">
        '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'
            
        </p>
        <hr>
     ';*/
        //$this->writeHTML($html, true, false, false, false, '');
		// Page number
		/*$this->Cell(0, 10, ' Avenida Baralt, esquina El Carmen. Edificio INN. Quinta Crespo, municipio Libertador.', 0, false, 'C', 0, '', 0, false, 'T', 'M');
	    $this->Ln();
        $this->Cell(0, 10, ' ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->Ln();*/
        $this->Cell(0, 10, $this->getAliasNumPage().' de '.$this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
        //$this->Cell(0, 11, '__________________________________________________________', 0, false, 'L', 0, '', 0, false, 'T', 'M');

         //$this->writeHTML('<hr>', true, false, false, false, '');
    }
    /*public function ColoredTable($header,$data) {
        // Colors, line width and bold font
        $this->SetFillColor(0, 0, 0);
        $this->SetTextColor(255);
        $this->SetDrawColor(0, 0, 0);
        $this->SetLineWidth(0.3);
        $this->SetFont('', 'B');
        // Header
        $w = array(40 ,55 , 60, 27,40, 25);
        $num_headers = count($header);
        for($i = 0; $i < $num_headers; ++$i) {
            $this->Cell($w[$i], 7, $header[$i], 1, 0, 'C', 1);
        }
        $this->Ln();
        // Color and font restoration
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetFont('');
        // Data
        $fill = 0;
        foreach($data as $row) {
			
			
        	$this->Cell($w[0], 6, $row['codigo'], 'LR', 0, 'L', $fill);
            $this->Cell($w[1], 6, $row['des_tipo_establecimiento'], 'LR', 0, 'L', $fill);
            $this->Cell($w[2], 6, $row['nombre'], 'LR', 0, 'L', $fill);
			$this->Cell($w[3], 6, $row['rif'], 'LR', 0, 'L', $fill);
			$this->Cell($w[4], 6, $row['des_tipo_denuncia'], 'LR', 0, 'L', $fill);
            $this->Cell($w[5], 6, $row['fecha'], 'LR', 0, 'L', $fill);
		
            $this->Ln();
            $fill=!$fill;
        }
        $this->Cell(array_sum($w), 0, '', 'T');
    }*/




}
?>
