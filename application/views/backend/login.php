<!DOCTYPE html>
<html lang="en">
<head>
<style>
button.btn.btn-primary.btn-block.btn-login {
    background-color: #f27e17 !important;
    border: 0px !important;
    height: 42px !important;
    font-size: 19px !important;
}
.cuar_form {
    width: 500px;
    margin-top: 302px;
    margin-left: auto;
    margin-right: auto;
}html, body {
        height: 100%;
        margin: 0px;
    }
.izq_q {
  overflow: visible;
  width: 50%;
  height: 95%;
  float: left;
  background-color: #ffffff;
}

.derech_q {
  width: 50%;
  float: right;
  background-color: #005aa5;
  height: 95%;
}

.full_footer {
      clear: both;
    height: 5%;
    background-color: #ee7e1a;
    padding-top: 16px;
    text-align: center;
    color: white;
    font-size: 12px;
}
.logo_o_f{
  width: 275px;
  height: 105px;
  float: right;
  margin-top: 348px;
  background-image:url(logo_p.jpg);
  background-repeat:no-repeat;
  margin-right: 26px;
}
</style>
	
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />
	
	<title><?php echo get_phrase('login');?> | <?php echo $system_title;?></title>
	

	<link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="assets/css/bootstrap.css">
	<link rel="stylesheet" href="assets/css/neon-core.css">
	<link rel="stylesheet" href="assets/css/neon-theme.css">
	<link rel="stylesheet" href="assets/css/neon-forms.css">
	<link rel="stylesheet" href="assets/css/custom.css">

	<script src="assets/js/jquery-1.11.0.min.js"></script>

	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<link rel="shortcut icon" href="assets/images/favicon.png">
	
</head>
<body class="page-body login-page login-form-fall" data-url="http://neon.dev">
<!-------- formulario de login -------->
  <div class="izq_q">  <div class="logo_o_f"></div>
</div>
  <div class="derech_q">
  <div class="cuar_form">
  
  <div class="login-form">
		
		<div class="login-content">
			
			<div class="form-login-error">
				<h3>Ingreso invalido</h3>
				<p>Por favor, introduzca el DNI y contraseña correcta!</p>
			</div>
			
			
			
			
			
			
		</div>
		
	</div>
  <form method="post" role="form" id="form_login">
				
				<div class="form-group">
					
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-user"></i>
						</div>
						
						<input type="text" class="form-control" name="" id="email" placeholder="DNI" autocomplete="off" data-mask="" />
					</div>
					
				</div>
				
				<div class="form-group">
					
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-key"></i>
						</div>
						
						<input type="password" class="form-control" name="" id="password" placeholder="Contraseña" autocomplete="off" />
					</div>
				
				</div>
				
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block btn-login">
						<i class="entypo-login"></i>
						INGRESAR
					</button>
                    
				</div>
				
						
			</form>
  <div class="login-bottom-links">
				<a href="<?php echo base_url();?>index.php?login/forgot_password" class="link">
					<?php echo get_phrase('forgot_your_password');?> ?
				</a>
			</div>
  </div>
  
  </div>
  <div class="full_footer"></div>
  <!-------- formulario de login -------->





<!-- This is needed when you send requests via Ajax -->
<script type="text/javascript">
var baseurl = '<?php echo base_url();?>';
</script>
























<div class="clear"></div>




	<!-- Bottom Scripts -->
	<script src="assets/js/gsap/main-gsap.js"></script>
	<script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="assets/js/bootstrap.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/neon-api.js"></script>
	<script src="assets/js/jquery.validate.min.js"></script>
	<script src="assets/js/neon-login.js"></script>
	<script src="assets/js/neon-custom.js"></script>
	<script src="assets/js/neon-demo.js"></script>

</body>
</html>