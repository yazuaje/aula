<style>
th.sorting_asc {
    color: white !important;

}
tr td {
    font-weight: bold;
    color: #000000 !important;
    font-size: 12px !important;
}
</style>
<div class="row">
	<div class="col-md-12">
    
    	<!------CONTROL TABS START------>
		<ul class="nav nav-tabs bordered">
			<li class="active">
            	<a href="#list" data-toggle="tab"><i class="entypo-menu"></i> 
					<?php echo get_phrase('invoice/payment_list');?>
                    	</a></li>
		</ul>
    	<!------CONTROL TABS END------>
		<div class="tab-content">
            <!----TABLE LISTING STARTS-->
            <div class="tab-pane box active" id="list">
				
                <table  class="table table-bordered datatable" id="table_export">
                	<thead>
                		<tr>
                            <th><div><?php echo get_phrase('date');?></div></th>
                    		<th><div><?php echo get_phrase('student');?></div></th>
                            <th><div><?php echo get_phrase('Clase');?></div></th>
                            <th><div><?php echo get_phrase('Sección');?></div></th> 
                    		<th><div><?php echo get_phrase('title');?></div></th>
                    		<th><div><?php echo get_phrase('description');?></div></th>
                    		<th><div><?php echo get_phrase('amount');?></div></th>
                    		<th><div><?php echo get_phrase('status');?></div></th>
                    		
						</tr>
					</thead>
                    <tbody>
                    	<?php foreach($invoices as $row):?>
                        <tr>
                            <td><?php echo date('d M,Y', $row['creation_timestamp']);?></td>

							<td><?php echo $this->crud_model->get_type_name_by_id('student',$row['student_id']);?></td>
                            <td>
                    <?php 
                        $class_id = $this->db->get_where('enroll' , array(
                            'student_id' => $row['student_id'],
                                'year' => $this->db->get_where('settings', array('type' => 'running_year'))->row()->description
                        ))->row()->class_id;
                        echo get_phrase('class') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->name;
                    ?>
                        
                    </td>
                        <td>
                    <?php 
                        $section_id = $this->db->get_where('enroll' , array(
                            'student_id' => $row['student_id'],
                                'year' => $this->db->get_where('settings', array('type' => 'running_year'))->row()->description
                        ))->row()->section_id;
                        echo get_phrase('') . ' ' . $this->db->get_where('section', array('section_id' => $section_id))->row()->name;
                    ?>
                        
                    </td>
							<td><?php echo $row['title'];?></td>
							<td><?php echo $row['description'];?></td>
							<td><?php echo $row['amount'];?></td>
							<td>
								<span class="label label-<?php if($row['status']=='Pagado')echo 'success';else echo 'unpaid';?>"><?php echo $row['status'];?></span>
							</td>
							
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
			</div>
            <!----TABLE LISTING ENDS--->
            
            
			
            
		</div>
	</div>
</div>

