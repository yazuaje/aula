  <a class="bt_atras" href="javascript:window.history.back();">Volver al Menú Principal</a>

<a href="http://www.nuevohorizonte.edu.pe/tutorial-aula-virtual-estudiantes/" target="_blank"><div class="cuadr_tutorial">Tutorial Aula Virtual</div></a>
<div class="cuadro_derech_1">

<div class="row">
	<div class="col-md-12">
    
    	<!------CONTROL TABS START------>
		<ul class="nav nav-tabs bordered">
			<li class="active">
            	<a href="#list" data-toggle="tab"><i class="entypo-menu"></i> 
					<?php echo get_phrase('subject_list');?>
                    	</a></li>
		</ul>
    	<!------CONTROL TABS END------>
		<div class="tab-content">            
            <!----TABLE LISTING STARTS-->
            <div class="tab-pane box active" id="list">
				
                <table class="table table-bordered datatable" id="table_export">
                	<thead>
                		<tr>
                    		<th><div><?php echo get_phrase('class');?></div></th>
                    		<th><div><?php echo get_phrase('subject_name');?></div></th>
                    		<th><div><?php echo get_phrase('teacher');?></div></th>
                            <th><div>nick modulo</div></th>
                    		<th><div>Libros</div></th>
                    		<th><div>Presentación</div></th>
						</tr>
					</thead>
                    <tbody>
                    	<?php $count = 1;foreach($subjects as $row):?>
                        <tr>
							<td><?php echo $this->crud_model->get_type_name_by_id('class',$row['class_id']);?></td>
							<td><?php echo $row['name'];?></td>
							<td><?php echo $this->crud_model->get_type_name_by_id('teacher',$row['teacher_id']);?></td>
							<td><?php echo $row['nick_modulo'];?></td>

                            <?php if(!$row['file_name'] == null):?>
							<td>
                                <a href="<?php echo base_url().'uploads/document/'.$row['file_name']; ?>" target="_blank" class="btn btn-blue btn-icon icon-left">
                                    <i class="entypo-download"></i>
                                    <?php echo get_phrase('download');?>
                                </a> 
                            </td>

                            <?php else: ?>

                            <td>
                                <a href="<?php echo base_url().'uploads/document/'.$row['file_name']; ?>" disabled target="_blank" class="btn btn-blue btn-icon icon-left">
                                    <i class="entypo-download"></i>
                                    <?php echo get_phrase('download');?>
                                </a> 
                            </td>
                            <?php endif ?>

							<?php if(!$row['file_name_2'] == null){?>
							<td>
                                <a href="<?php echo base_url().'uploads/document/'.$row['file_name_2']; ?>" target="_blank" class="btn btn-blue btn-icon icon-left">
                                    <i class="entypo-download"></i>
                                    <?php echo get_phrase('download');?>
                                </a> 
                            </td>

                            <?php }else{ ?>

                            <td>
                                <a href="<?php echo base_url().'uploads/document/'.$row['file_name_2']; ?>" disabled target="_blank" class="btn btn-blue btn-icon icon-left">
                                    <i class="entypo-download"></i>
                                    <?php echo get_phrase('download');?>
                                </a> 
                            </td>
                            <?php } ?>
							
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
			</div>
            <!----TABLE LISTING ENDS-->
            
            
			
            
		</div>
	</div>
</div>
</div>


<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">

	jQuery(document).ready(function($)
	{
		

		var datatable = $("#table_export").dataTable();
		
		$(".dataTables_wrapper select").select2({
			minimumResultsForSearch: -1
		});
	});
		
</script>>>>>