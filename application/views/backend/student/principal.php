
<style>
a.bt_libr_sub_admin {
    display: none !important;
}
ul.nav.nav-tabs.bordered {
    display: none !important;
}
div#modalInicio {
	background-color: rgba(0, 0, 0, 0.33);
}
.modal-backdrop.in {
	position: relative !important;
}
.sidebar-menu {
	display: none !important;
}
iframe {
	border: 0px;
}
h3 {
	display: none !important;
}
ul.list-inline.links-list.pull-left {
	display: none;
}
.simple-popup-content {
    width: 700px !important;
    padding: 14px !important;
}

input#required {
    font-size: 14px;
    color: black;
    font-weight: 700;
    border: 1px solid #b9b9b9;
    width: 100%;
    border-radius: 7px;
    padding: 7px;
}

label {
    font-size: 15px;
    color: #005aa6;    padding-top: 5px;
      padding-top: 13px;
    padding-left: 4px;
}

select {
    font-size: 11px;
    color: black;
    font-weight: 700;
    border: 1px solid #b9b9b9;
    width: 100%;
    border-radius: 7px;
    padding: 7px;
}

textarea#message {
    font-size: 14px;
    color: black;
    font-weight: 700;
    border: 1px solid #b9b9b9;
    width: 100%;
    border-radius: 7px;
    padding: 7px;
}

input#Consulta {
    font-size: 14px;
    color: black;
    font-weight: 700;
    border: 1px solid #b9b9b9;
    width: 100%;
    border-radius: 7px;
    padding: 7px;
}
button#send {
    background-color: #005aa6;
    color: white;
    padding: 10px 20px 10px 20px;
    border: 0px;
}
button#send:hover {
    background-color: #003867;
}
button {
    background-color: #005aa6;
    color: white;
    padding: 10px 20px 10px 20px;
    border: 0px;
}
.titulo_formulario {
    font-size: 21px;
    font-weight: bold;
    text-align: center;
    color: #005aa6;
    border-bottom: 1px solid #cacaca;
    padding-bottom: 13px;
}
</style>
  <link href="assets/js/popup/jquery.simple-popup.min.css" rel="stylesheet" type="text/css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<?php
  # Iniciando la variable de control que permitirá mostrar o no el modal
  $exibirModal = false;
  # Verificando si existe o no la cookie
  if(!isset($_COOKIE["mostrarModal"]))
  {
    # Caso no exista la cookie entra aqui
    # Creamos la cookie con la duración que queramos
     
    //$expirar = 3600; // muestra cada 1 hora
    //$expirar = 10800; // muestra cada 3 horas
    //$expirar = 21600; //muestra cada 6 horas
    $expirar = 15552000; //muestra cada 12 horas
    //$expirar = 86400;  // muestra cada 24 horas
    setcookie('mostrarModal', 'SI', (time() + $expirar)); // mostrará cada 12 horas.
    # Ahora nuestra variable de control pasará a tener el valor TRUE (Verdadero)
    $exibirModal = true;
  }
?>

<div class="container">
  <div id="popup1">
 <div class="titulo_formulario"> ATENCIÓN AL CLIENTE</div>
  
                        <?php foreach($courses as $course) { ?>

    <form action="enviar_formulario_cc.php" method="post" id="contact" name="contact">
      <fieldset>
        <label>Nombres y Apellidos :</label>
        <div class="clear1112"></div>
        <input name="name" type="text" class="required" id="required" value="<?php echo $this->session->userdata('name');?>" readonly="readonly" minlength="2"/>
        <div class="clear"></div>
        <label>Curso : </label>
        <div class="clear1112"></div>
<select name="">
  <option value="<?php echo $course['name']; ?>"><?php echo $course['name']; ?></option>
</select>  
<div class="clear"></div>
        <label>Correo :</label>
        <div class="clear1112"></div>
        <input type="text" id="Consulta" name="Consulta" class="required" minlength="2"/>      
<div class="clear"></div>
        <label>Celular :</label>
        <div class="clear1112"></div>
        <input type="text" id="Consulta" name="Consulta" class="required" minlength="2"/>   
<div class="clear"></div>
        <label>Asunto : </label>
        <div class="clear1112"></div>
        <input type="text" id="Consulta" name="Consulta" class="required" minlength="2"/>
        <div class="clear"></div>
        <label>Mensaje : </label>
        <div class="clear1112"></div>
        <textarea id="message" name="message" cols="39" rows="5" class="required"></textarea>
        <div class="clear"></div>
        </br>
        <button type="submit" id="send" name="send"> ENVIAR </button>
        <button type="reset" id="" name=""> LIMPIAR </button>
      </fieldset>
    </form>
    
                            <?php } ?>

  </div>
</div>
<div class="row">
<div class="col-md-12">
<!------CONTROL TABS START------>
<ul class="nav nav-tabs bordered">
  <li class="active"> <a href="#list" data-toggle="tab"><i class="entypo-menu"></i> <?php echo get_phrase('Clases');?> </a></li>
</ul>
<div class="modal fade" id="modalInicio" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="cabezera_1">
          <div class="logo_nuevo_h"></div>
        </div>
        <div class="centro_pop">
          <div class="nombre_pop"><span style="color:#005aa6; font-weight:bold;">Bienvenido(a) :</span> <?php echo $this->session->userdata('name');?></div>
          <div class="cuerp_pop"> Le damos la bienvenida a nuestra institución y a su aula virtual, en ella podrá acceder a las presentaciones de las ponencias, ver su cronograma de capacitación, verificar su registró de asistencia, calificación de sus trabajos, descarga de libros virtuales, audio libros, informes técnicos y tutoriales especializados. </div>
          <div class="cuerp_pop"> Le Recomendamos verificar sus Datos Personales:</div>
          <br/>
          <a href="index.php?student/manage_profile">
          <div class="verificar_infor">Verificar Datos Personales</div>
          </a> </div>
      </div>
    </div>
  </div>
</div>
<!------CONTROL TABS END------>
<div class="tab-content">
<!----TABLE LISTING STARTS-->
            <div class="tab-pane box active" id="list">
                <div class="c_izq_home">
                <br>
                    <div class="title_home_1"><span style="color:#005aa6;">MIS</span> CURSOS</div>
                    <div class="c_item_cursos">
                        <?php foreach($courses as $course) { ?>
                    <!----------------------item 1---------------------->
                        <div class="c_item_cursos_item1">
                            <div class="title_class"><?php echo $course['name']; ?></div>
                            <div class="title_year"><?php echo  $this->db->get_where('settings' , array('type' => 'running_year'))->row()->description; ?></div>
                            <a href="<?php echo base_url() . 'index.php?student/dashboard/'.$course['class_id']; ?>"><div class="bt_entrar">ENTRAR</div></a>
                            <div class="clear1"></div>

                        </div>
                        <?php } ?>
                </div>   
            </div>    
            
            <div class="c_derc_home w-clearfix">
      <div class="title_home_1"><span style="color:#005aa6;">CURSOS</span> DISPONIBLES</div>
      <div class="full_slider">
      
<iframe src="slider_pagina_principal/slider.html" width="100%" height="300px" scrolling="no"></iframe>    <!-- End of body section HTML codes -->
      </div>
      <div class="c_izq_drec_home">
        <div class="c_cuadr_text_home">
          <div class="text_c_peq">TUTORIAL</div>
          <div class="text2_cc text_c_peq">AULA VIRTUAL</div>
          <a target="_blank" href="http://www.nuevohorizonte.edu.pe/tutorial/"><div class="bt_entrar_2">ENTRAR</div></a>
        </div>
      </div>
      <div class="c_izq_drec_home derec_ff">
        <div class="c_cuadr_text_home">
          <div class="text_c_peq">ATENCIÓN</div>
          <div class="text2_cc text_c_peq">AL CLIENTE</div>
          <a class="demo-2" href="#"><div class="bt_entrar_2">ENTRAR</div></a>
        </div>
      </div>
    </div>
        
        </div>
    </div>    
    
    
</div>      






<?php if($exibirModal === true) : // Si nuestra variable de control "$exibirModal" es igual a TRUE activa nuestro modal y será visible a nuestro usuario. ?>
<script>
$(document).ready(function()
{
  // id de nuestro modal
  $("#modalInicio").modal("show");
});
</script>
<?php endif; ?>
<script src="assets/js/popup/jquery.simple-popup.min.js"></script>
<script>
$(document).ready(function() {
  $("a.demo-1").simplePopup();
  $("a.demo-2").simplePopup({ type: "html", htmlSelector: "#popup1" });
});
</script>
