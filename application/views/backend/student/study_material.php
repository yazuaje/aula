  <a class="bt_atras" href="javascript:window.history.back();">Volver al Menú Principal</a>

<a href="http://www.nuevohorizonte.edu.pe/tutorial-aula-virtual-estudiantes/" target="_blank"><div class="cuadr_tutorial">Tutorial Aula Virtual</div></a>
<script>
    // Sort by 3rd column first, and then 4th column
		   $(document).ready( function() {
		     $('#example').dataTable( {
		        "order": [[0,'desc']]
		      } );
		    } );
</script>
<div class="cuadro_derech_1">

<table class="table table-bordered table-striped datatable" id="example">
    <thead>
        <tr>
            <th>#</th>
<?php /*?>            <th><?php echo get_phrase('date');?></th>
<?php */?>            <th><?php echo get_phrase('title');?></th>
            <th><?php echo get_phrase('description');?></th>
            <th><?php echo get_phrase('class');?></th>
            <th><?php echo get_phrase('download');?></th>
        </tr>
    </thead>

    <tbody>
        <?php 
        $count = 1;
        foreach ($study_material_info as $row) { ?>   
            <tr>
                <td><?php echo $count++; ?></td>
<?php /*?>                <td><?php echo date("d M, Y", $row['timestamp']); ?></td>
<?php */?>                <td><?php echo $row['title']?></td>
                <td><?php echo $row['description']?></td>
                <td>
                    <?php $name = $this->db->get_where('class' , array('class_id' => $row['class_id'] ))->row()->name;
                        echo $name;?>
                </td>
                <td>
                    <a target="_blank" href="<?php echo base_url().'uploads/document/'.$row['file_name']; ?>" class="btn btn-blue btn-icon icon-left">
                        <i class="entypo-download"></i>
                        Descargar
                    </a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
</div>

<script type="text/javascript">
    jQuery(window).load(function ()
    {
        var $ = jQuery;

        $("#table-2").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>"
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });

        // Highlighted rows
        $("#table-2 tbody input[type=checkbox]").each(function (i, el)
        {
            var $this = $(el),
                    $p = $this.closest('tr');

            $(el).on('change', function ()
            {
                var is_checked = $this.is(':checked');

                $p[is_checked ? 'addClass' : 'removeClass']('highlight');
            });
        });

        // Replace Checboxes
        $(".pagination a").click(function (ev)
        {
            replaceCheckboxes();
        });
    });
</script>