<style>
.sidebar-menu {
    display: none !important;
}
a.bt_libr_sub_admin {
    display: none !important;
}

</style>
<div class="sidebar-menu">
    <header class="logo-env" >

        <!-- logo -->
    <!--    <div class="logo" style="">
            <a href="<?php echo base_url(); ?>">
                <img src="uploads/logo.png"  style="max-height:60px;"/>
            </a>
        </div>-->

        <!-- logo collapse icon -->
        <div class="sidebar-collapse" style="">
            <a href="#" class="sidebar-collapse-icon with-animation">

                <i class="entypo-menu"></i>
            </a>
        </div>

        <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
        <div class="sidebar-mobile-menu visible-xs">
            <a href="#" class="with-animation">
                <i class="entypo-menu"></i>
            </a>
        </div>
    </header>

    <div style="border-top:1px solid rgba(69, 74, 84, 0.7);"></div>	
    <ul id="main-menu" class="">
        <!-- add class "multiple-expanded" to allow multiple submenus to open -->
        <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->


        <!-- DASHBOARD -->
       <?php /*?> <li class="<?php if ($page_name == 'dashboard') echo 'active'; ?> ">
<a href="<?php echo base_url(); ?>index.php?<?php echo $account_type; ?>/principal">
                <i class="entypo-gauge"></i>
                <span><?php echo get_phrase('dashboard'); ?></span>
            </a>
        </li>



        <!-- TEACHER -->
        <li class="<?php if ($page_name == 'teacher') echo 'active'; ?> ">
<a href="<?php echo base_url() . 'index.php?student/teacher_list/personal_profile/'.$class_id; ?>">

                <i class="entypo-users"></i>
                <span><?php echo get_phrase('teacher'); ?></span>
            </a>
        </li>



        <!-- SUBJECT -->
        <li class="<?php if ($page_name == 'subject') echo ' active'; ?> ">
<a href="<?php echo base_url() . 'index.php?student/subject/'.$class_id; ?>">
            
            
            
            
                <i class="entypo-docs"></i>
                <span><?php echo get_phrase('subject'); ?></span>
            </a>
        </li>

        <!-- CLASS ROUTINE -->
        <li class="<?php if ($page_name == 'class_routine') echo 'active'; ?> ">
<a href="<?php echo base_url() . 'index.php?student/class_routine/'.$class_id; ?>">
                <i class="entypo-target"></i>
                <span><?php echo get_phrase('class_routine'); ?></span>
            </a>
        </li>
     <!-- Exam marks -->
        <li class="<?php if ($page_name == 'student_marksheet') echo 'active'; ?> ">
<a href="<?php echo base_url() . 'index.php?student/student_marksheet/'.$this->session->userdata('login_user_id').'/'.$class_id; ?>">
                <i class="entypo-graduation-cap"></i>
                <span><?php echo get_phrase('exam_marks'); ?></span>
            </a>
        </li>

        <!-- Attendance -->
        <li class="<?php if ($page_name == 'attendance_report_student') echo 'active'; ?> ">
<a href="<?php echo base_url() . 'index.php?student/manage_attendance_report_student/'.$this->session->userdata('login_user_id').'/'.$class_id; ?>">
                <i class="entypo-chart-bar"></i>
                <span><?php echo get_phrase('Asistencia'); ?></span>
            </a>
        </li>
        
		<!-- STUDY MATERIAL -->
        <li class="<?php if ($page_name == 'study_material') echo 'active'; ?> ">
<a href="<?php echo base_url() . 'index.php?student/study_material/'.$class_id; ?>">
                <i class="entypo-book-open"></i>
                <span><?php echo get_phrase('study_material'); ?></span>
            </a>
        </li>

         <!--Diapositivas-->
        <li class="<?php if ($page_name == 'Diapositivas') echo 'active'; ?> ">
<a href="<?php echo base_url() . 'index.php?student/study_slide/'.$class_id; ?>">
                    <i class="entypo-book-open"></i>
                    <span><?php echo 'Diapositivas'; ?></span>
                </a>
            </li>
<?php */?>
        <!-- ACADEMIC SYLLABUS -->
       <?php /*?> <li class="<?php if ($page_name == 'academic_syllabus') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?student/academic_syllabus/<?php echo $this->session->userdata('login_user_id');?>">
                <i class="entypo-doc"></i>
                <span><?php echo get_phrase('academic_syllabus'); ?></span>
            </a>
        </li><?php */?>

   
        <!-- PAYMENT -->
        <li class="<?php if ($page_name == 'invoice') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?<?php echo $account_type; ?>/invoice">
                <i class="entypo-credit-card"></i>
                <span><?php echo get_phrase('payment'); ?></span>
            </a>
        </li>


        <!-- LIBRARY -->
<?php /*?>        <li class="<?php if ($page_name == 'book') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?<?php echo $account_type; ?>/book">
                <i class="entypo-book"></i>
                <span><?php echo get_phrase('library'); ?></span>
            </a>
        </li><?php */?>

        <!-- TRANSPORT -->
    <?php /*?>    <li class="<?php if ($page_name == 'transport') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?<?php echo $account_type; ?>/transport">
                <i class="entypo-location"></i>
                <span><?php echo get_phrase('transport'); ?></span>
            </a>
        </li>
<?php */?>
        <!-- NOTICEBOARD -->
        <?php /*?><li  class="<?php if ($page_name == 'noticeboard') echo 'active'; ?> ">
            <a target="_blank" href="<?php echo base_url(); ?>index.php?<?php echo $account_type; ?>/noticeboard">
                <i class="entypo-doc-text-inv"></i>
                <span><?php echo get_phrase('noticeboard'); ?></span>
            </a>
        </li>

        <!-- MESSAGE -->
        <li  class="<?php if ($page_name == 'message') echo 'active'; ?> ">
            <a target="_blank" href="<?php echo base_url(); ?>index.php?<?php echo $account_type; ?>/message">
            
                <i class="entypo-mail"></i>
                <span><?php echo get_phrase('message'); ?></span>
                
            </a>
            
        </li>

        <!-- ACCOUNT -->
        <li class="<?php if ($page_name == 'manage_profile') echo 'active'; ?> ">
            <a target="_blank" href="<?php echo base_url(); ?>index.php?<?php echo $account_type; ?>/manage_profile">
                <i class="entypo-lock"></i>
                <span><?php echo get_phrase('account'); ?></span>
            </a>
        </li>
<?php */?>
    </ul>

</div>






