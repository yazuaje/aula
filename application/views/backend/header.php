<div class="row">
	<div class="col-md-12 col-sm-12 clearfix" style="text-align:center;">
    
		<h2 style="font-weight:200; margin:0px;"><?php /*?><?php echo $system_name;?><?php */?></h2>
    </div>
	<!-- Raw Links -->
	<div class="col-md-12 col-sm-12 clearfix header_full">
		<div class="logo_header_p"><img src="assets/images/logo_pp.png" width="200" height="76" /></div>
        
          <a class="bt_libr_sub_admin" href="<?php echo base_url(); ?>index.php?admin/ultimos_works">Nuevos Trabajos Subidos</a>

        
        
          <div class="c_redes_11"><div class="redes_nh">
<a target="_blank" href="https://www.facebook.com/institucionnuevohorizonte"><div class="ico_redes_nh"></div></a>
<a target="_blank" href="https://www.youtube.com/channel/UChgORIQy7GqEgrILTFlzndA"><div class="ico_redes_nh_you"></div></a>
<a target="_blank" href="#"><div class="ico_redes_nh_tw"></div></a>

</div></div>
        
        
        
        <ul class="list-inline links-list pull-left">
        <!-- Language Selector -->
        	<div id="session_static">			
	           <li>
	           		<div class="anio_curs">                    <?php echo get_phrase('class');?> - <?php echo $this->db->get_where('class' , array('class_id' => $class_id))->row()->name;?> : 
</br>
	           			<a href="#" style="color: #FFF;"
	           				<?php if($account_type == 'admin'):?> 
	           				onclick="get_session_changer()"
	           			<?php endif;?>>
	           				<?php echo get_phrase('running_session');?> : <?php echo $running_year;?>
	           			</a>
	           		</div>
	           </li>
           </div>
        </ul>
        
        
		<ul class="list-inline links-list pull-right">

		<li class="dropdown language-selector">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true">
                        	<div class="ico_people"></div>
                            <div class="text_blanco">
                             <?php echo $this->session->userdata('name');?></div>
                    </a>

				<?php if ($account_type != 'parent'):?>
				<ul class="dropdown-menu <?php if ($text_align == 'right-to-left') echo 'pull-right'; else echo 'pull-left';?>">
					<li>
						<a href="<?php echo base_url();?>index.php?<?php echo $account_type;?>/manage_profile">
                        	<i class="entypo-info"></i>
							<span><?php echo get_phrase('edit_profile');?></span>
						</a>
					</li>
					<li>
						<a href="<?php echo base_url();?>index.php?<?php echo $account_type;?>/manage_profile">
                        	<i class="entypo-key"></i>
							<span><?php echo get_phrase('change_password');?></span>
						</a>
					</li>
				</ul>
				<?php endif;?>
				<?php if ($account_type == 'parent'):?>
				<ul class="dropdown-menu <?php if ($text_align == 'right-to-left') echo 'pull-right'; else echo 'pull-left';?>">
					<li>
						<a href="<?php echo base_url();?>index.php?parents/manage_profile">
                        	<i class="entypo-info"></i>
							<span><?php echo get_phrase('edit_profile');?></span>
						</a>
					</li>
					<li>
						<a href="<?php echo base_url();?>index.php?parents/manage_profile">
                        	<i class="entypo-key"></i>
							<span><?php echo get_phrase('change_password');?></span>
						</a>
					</li>
				</ul>
				<?php endif;?>
			</li>
			<div class="cuadr_salir">
			<div class="linea_header"></div>

			<li>                            
<div class="text_blanco_a">
				<a href="<?php echo base_url();?>index.php?login/logout">
					Salir </a>	</div><div class="ico_salir"></div>
			</li></div>
		</ul>
	</div>
	
</div>

<hr style="margin-top:0px;" />

<script type="text/javascript">
	function get_session_changer()
	{
		$.ajax({
            url: '<?php echo base_url();?>index.php?admin/get_session_changer/',
            success: function(response)
            {
                jQuery('#session_static').html(response);
            }
        });
	}
</script>