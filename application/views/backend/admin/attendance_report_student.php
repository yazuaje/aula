<hr />


<div class="linea divisor1"></div>


<!-- ----------------FORMULARIO DE ASISTECIA----------------------- -->

<div class="row">
	<div class="col-md-12">
        	<?php
			$class_name = $this->db->get_where('class' , array('class_id' => $class_id ))->row()->name;
			$section_name = $this->db->get_where('section' , array('class_id' => $class_id , 'section_id' => $section_id))->row()->name;
		 ?>
		<p class='text text-center'> <b>Clase:</b> <?php echo $class_name; ?></p>
		<p class='text text-center'> <b>Seccion:</b> <?php echo $section_name; ?></p>
		<div id="dvData">
		<table class="table table-bordered">
			<thead>
			<?php
					$teacher = $this->db->get_where('section' , array('class_id' => $class_id , 'section_id' => $section_id))->row()->teacher_id;
					$subjects = $this->db->get_where('subject' , array('class_id' => $class_id , 'teacher_id' => $teacher))->result_array();
					// $enrolls = $this->db->get_where('enroll' , array('class_id' => $class_id , 'section_id' => $section_id))->result_array();
			?>
			<tr>
				<td></td>
				<td style="text-align: center;" colspan="<?php echo count($subjects) ?>"><b><?php echo get_phrase('control_de_asistencia');?></b></td>
				<!--<td  style="text-align: center;" colspan="<?php echo count($subjects) ?>"><b><?php echo get_phrase('control_de_trabajos');?></b></td>-->
				<!--<td></td>-->

			</tr>
				<tr>
				<td style="text-align: center;"><?php echo get_phrase('Estudiantes| /Fechas-');?></td>
				<?php $i=1; foreach($subjects as $row): ?>
          <?php
            $class_routine_date = 	$this->db->get_where('class_routine' , array(
													                                  'class_id' => $class_id ,
															                              'section_id' => $section_id ,
																                            'subject_id' => $row['subject_id'],
																	                          'year' => $running_year
												                                  ))->row()->date; 
          ?>
					<td style="text-align: center;">
            <p><?php echo $row['nick_modulo'];?></p>
            <p><?php echo $class_routine_date;?></p>
          </td>
				<?php $i++; endforeach; ?>
				</tr>
			</thead>
			<tbody>
			<?php
				foreach($enrolls as $row):
			?>
				<tr>
					<td style="text-align: center;">
						<?php echo $this->db->get_where('student' , array('student_id' => $row['student_id']))->row()->name;?>
					</td>
          <!--<td></td>-->
				<?php
					foreach($subjects as $row2):
				?>
					<td style="text-align: center;">
						<?php
							  $class_routine_id = 	$this->db->get_where('class_routine' , array(
													                                  'class_id' => $row['class_id'] ,
															                              'section_id' => $row['section_id'] ,
																                            'subject_id' => $row2['subject_id'],
																	                          'year' => $running_year
												                                  ))->row()->class_routine_id;

							$status = 	$this->db->get_where('attendance' , array(
													                      'class_id' => $row['class_id'] ,
															                  'section_id' => $row['section_id'] ,
																                'student_id' => $row['student_id'],
																                'class_routine_id' => $class_routine_id,
																	              'year' => $running_year
												                      ))->row()->status;
                                              //  var_dump($row['class_id']);
                                              //  var_dump($row['section_id']);
                                              //  var_dump($row['student_id']);
                                              //  var_dump($row2['subject_id']);
                                              //  echo "class_routine ".$class_routine_id;
                                              // echo $status;
						?>
            <div class="row">
              <?php if($status == 1){ ?>
                <p class="text text-center"> Presente </p>
			  <?php } elseif ($status == 2){ ?>
                <p class="text text-center"> Ausente </p>
			  <?php } else{ ?>
				<p class="text text-center"> Indefinido </p>
			  <?php }?>
					</td>
				<?php endforeach;?>
				</tr>
			<?php endforeach;?>

			</tbody>
		</table>
		</div>
	</div>
</div>

<!--<center>
	<a href="<?php echo base_url();?>index.php?admin/attendance_print_view/<?php echo $class_id;?>/<?php echo $section_id;?>"
				class="btn btn-primary" target="_blank">
				<?php echo get_phrase('inprimir pdf');?>
			</a>
			<a href="#"
				class="btn btn-primary" id="buttonExcel" target="_blank">
				<?php echo get_phrase('inprimir excel');?>
			</a>
		</center>-->

<!-- ----------------FORMULARIO DE ASISTECIA----------------------- -->

<script>
		$("#buttonExcel").click(function (e) 
		{
    		$(".table").table2excel({
					exclude: ".noExl",
					name: "Excel Document Name",
					filename: "asistencia",
					fileext: ".xls",
					exclude_img: true,
					exclude_links: true,
					exclude_inputs: true
				});
		});
</script>