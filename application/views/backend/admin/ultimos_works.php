<style>
a.btn.btn-blue.btn-icon.icon-left {
    background-color: red !important;
}

i.entypo-download {
    background-color: #ce0404 !important;
}
</style>
<div style="clear:both;"></div>
<br>

<?php echo form_open(base_url() . 'index.php?admin/ultimos_works_update');?>
<table class="table table-bordered table-striped datatable" id="example">
    <thead>
        <tr>
            <th>#</th>
<th><?php echo get_phrase('name');?></th>
<th><?php echo get_phrase('class');?></th>
<th><?php echo get_phrase('section');?></th>
<th><?php echo get_phrase('subject');?></th>
<th>Trabajos</th>
						<th><?php echo get_phrase('marks_obtained');?></th>
						<th><?php echo get_phrase('comment');?></th>
</tr>

</thead>
 <tbody>

<?php
					$count = 1;
					$marks_of_students = $this->db->order_by('mark_id','DESC')->limit(100)->get_where('mark',array('mark_obtained=' => '0','file_name!='=>''))->result_array(); 
	foreach($marks_of_students as $row):
				?>
  <tr>
	<td><?php echo $count++;?></td>
						 
						<td>
							<?php echo $this->db->get_where('student' , array('student_id' => $row['student_id']))->row()->name;?>
						</td>
<td><?php echo $class=$this->db->get_where('class' , array('class_id' => $row['class_id']))->row()->name;?></td>
<td><?php echo $section=$this->db->get_where('section' , array('section_id' => $row['section_id']))->row()->name;?></td>
 
<input type="hidden" class="form-control" name="mark_id[]" value="<?php print $row['mark_id'] ?>">

<td><?php echo $this->db->get_where('subject' , array('subject_id' => $row['subject_id']))->row()->name;?></td>
<td><?php if($row['file_name']!=null){ ?>
							<a href="<?php echo base_url().'uploads/document/'.$row['file_name']; ?>" target="_blank" class="btn btn-blue btn-icon icon-left">
                                    <i class="entypo-download"></i>
                                    <?php echo get_phrase('download');?>
                                </a>
                          <?php } else { ?>
                          <a   class="btn btn-default icon-left">
                                    <i class="entypo-download"></i>
                                    <?php echo get_phrase('download');?>
                                </a>
							<?php } ?></td>
<td>
							<input type="text" class="form-control" name="mark[]" required value="0">
						</td>
						<td>
							<input type="text" class="form-control" name="comment[]">
						</td>
</tr>
	<?php endforeach;?>
 
 </tbody>

</table>
	<center>
			<button type="submit" class="btn btn-success" id="submit_button">
				<i class="entypo-check"></i> <?php echo get_phrase('save_changes');?>
			</button>
		</center>
</form>
<script type="text/javascript">

	jQuery(document).ready(function($)
	{
		

		var datatable = $("#table_export").dataTable();
		
		 
	});
		
</script>



<script>
    // Sort by 3rd column first, and then 4th column
		   $(document).ready( function() {
		     $('#example').dataTable( {
"lengthMenu": [ [-1], [ "FULL"] ],
 "order": [[0,'asc']]				
		      } );
		    } );
</script>






<script type="text/javascript">
    jQuery(window).load(function ()
    {
        var $ = jQuery;

        $("#table-2").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>"
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });

        // Highlighted rows
        $("#table-2 tbody input[type=checkbox]").each(function (i, el)
        {
            var $this = $(el),
                    $p = $this.closest('tr');

            $(el).on('change', function ()
            {
                var is_checked = $this.is(':checked');

                $p[is_checked ? 'addClass' : 'removeClass']('highlight');
            });
        });

        // Replace Checboxes
        $(".pagination a").click(function (ev)
        {
            replaceCheckboxes();
        });
    });
</script>
   
