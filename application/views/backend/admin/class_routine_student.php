<style>
tr.tab123 {
    background-color: #d8d8d8;
    color: white !important;
    text-align: center;
    font-size: 21px;
    font-weight: bold;
}
table.tb147 tr td {
    text-align: center;
    color: #4a4a4a !important;
    font-weight: bold;
    padding: 10px;
    font-size: 15px;
    border: 1px solid #ccc;
}
a.btn.btn-primary.btn-xs.pull-right {
    background-color: #4faeff;
    padding: 3px;
    padding-left: 10px;
    padding-right: 11px;
}
td.tb_w {
    color: #2b2b2b !important;
    font-size: 16px;
}
.align-center td{
text-align: center;
}
.align-center td:last-child{
border-right: none;
    }
	.clase_text{ text-transform:capitalize; }
</style>
<!--<a href="http://www.nuevohorizonte.edu.pe/tutorial-aula-virtual-estudiantes/" target="_blank"><div class="cuadr_tutorial">Tutorial Aula Virtual</div></a>-->
<hr />
<?php
    $section_id = $this->db->get_where('enroll' , array(
        'student_id' => $student_id,
            'class_id' => $class_id,
                'year' => $running_year
    ))->row()->section_id;
?>

<div class="row">
    
    <div class="col-md-12">

        <div class="panel panel-default" data-collapsed="0">
            <div class="panel-heading" >
                <div class="panel-title" style="font-size: 16px; color: white; text-align: center;">
                    <?php echo get_phrase('class');?> - <?php echo $this->db->get_where('class' , array('class_id' => $class_id))->row()->name;?> : 
                    <?php echo get_phrase('section');?> - <?php echo $this->db->get_where('section' , array('section_id' => $section_id))->row()->name;?>
                    <a href="<?php echo base_url();?>index.php?admin/class_routine_print_view_student/<?php echo $class_id;?>/<?php echo $section_id;?>" 
                        class="btn btn-primary btn-xs pull-right" target="_blank">
                            <i class="entypo-print"></i> <?php echo get_phrase('print');?>
                    </a>
                </div>
            </div>
            <!-- TABLA PARA MODIFICAR -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tb147">
<tr class="tab123">
<td class="tb_w" width="30%">Modulo</td>
<td class="tb_w" width="10%">Dia</td>
<td class="tb_w" width="10%">Fecha</td>
<td class="tb_w" width="10%">Hora</td>
<td class="tb_w" width="22%">Lugar</td>


</tr>
<!--<tr>
<td>Marco Legal, Organizacional 
y Administrativo de la Gesti¨®n 
P¨²blica Municipal. </td>
<td>Lunes </td>
<td>25 de febrero</td>
<td>8:00 AM </br>
12:00  PM</td>
<td>Universidad Naciona de Trujillo</td>
<td>Boton Editar / eliminar</td>
</tr>-->
</table>
<!-- TABLA PARA MODIFICAR -->
            <div class="">
                
                <table cellpadding="0" cellspacing="0" border="0"  class="table table-bordered">
                    <tbody>
                        <?php 
                        for($d=1;$d<=7;$d++):
                        
                        if($d==1)$day='Domingo';
                        else if($d==2)$day='Lunes';
                        else if($d==3)$day='Martes';
                        else if($d==4)$day='Miercoles';
                        else if($d==5)$day='Jueves';
                        else if($d==6)$day='Viernes';
                        else if($d==7)$day='Sabado';
                        ?>
                        <tr  class="gradeA">
<?php
$this->db->order_by("time_start", "asc");
$this->db->where('day' , $day);
$this->db->where('class_id' , $class_id);
$this->db->where('section_id' , $section_id);
$this->db->where('year' , $running_year);
$routines   =   $this->db->get('class_routine')->result_array();
foreach($routines as $row2):
?>
<td  class="clase_text" style=" vertical-align:middle;   text-align: center;
    font-size: 12px;" width="30%">
<?php echo $this->crud_model->get_subject_name_by_id($row2['subject_id']);?>
</td>
                        
                  <td  class="clase_text" style="vertical-align:middle;    text-align: center;
    font-size: 12px;"  width="10%"><?php echo ($day);?>
                  </td>
                   
                  <td  class="clase_text" style="vertical-align:middle;    text-align: center;
    font-size: 12px;"  width="10%"><?php echo ($row2['date']);?>
                  </td>
                   
                  <td  class="clase_text" style=" vertical-align:middle;   text-align: center;
    font-size: 12px;" width="10%"><?php
                                        $changetime = 12;
                                            if ($row2['time_start_min'] == 0 && $row2['time_end_min'] == 0){
                                                echo $row2['time_start'].' : 00 AM<br>';
                                            echo $row2['time_end']-$changetime.' : 00 PM';
                                            }
                                            else{
                                         echo $row2['time_start'].':'.$row2['time_start_min'].' AM<br>'; 
                                          echo $row2['time_end']-$changetime.':'.$row2['time_end_min'].' PM';
                                        } ?>
                  </td>
                   
                  <td   class="clase_text" style=" vertical-align:middle;   text-align: center;
    font-size: 12px;"  width="22%"><?php echo ($row2['place']);?>
                  </td>      
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     
                          
                        </tr><?php endforeach;?>
                        <?php endfor;?>
                        
                    </tbody>
                </table>
                
          </div>
        </div>

    </div>

</div>