<style>
a.form-control.inscribir.inline.btn.btn-primary {
    background-color: red !important;
    border: 1px solid red !important;
}
td select {
    width: 100%;
    padding: 10px;
    font-weight: bold;
    font-size: 13px;
}
</style>
<div class="izq_cuer_ord">

<a href="http://www.nuevohorizonte.edu.pe/tutorial-aula-virtual-estudiantes/" target="_blank"><div class="cuadr_tutorial">Tutorial Aula Virtual</div></a>

<div class="row">

	<div class="col-md-12">

    

    	<!------CONTROL TABS START------>

		<ul class="nav nav-tabs bordered">

			<li class="active">

            	<a href="#list" data-toggle="tab"><i class="entypo-menu"></i> 

					<?php echo get_phrase('subject_list');?>

                    	</a></li>
                <li>

                    <a href="#inscribir" data-toggle="tab"><i class="entypo-menu"></i> 

                    <?php echo get_phrase('inscribir');?>

                    </a>
                </li>            

		</ul>

    	<!------CONTROL TABS END------>

		<div class="tab-content">            

            <!----TABLE LISTING STARTS-->

            <div class="tab-pane box active" id="list">
                <table class="table table-bordered datatable" id="table_export">

                	<thead>

                		<tr>

                    		<th><div><?php echo get_phrase('class');?></div></th>

                    		<th><div><?php echo get_phrase('subject_name');?></div></th>

                    		<th><div><?php echo get_phrase('teacher');?></div></th>

                            <th><div>nick modulo</div></th>

                    		<th><div>Archivo 1</div></th>

                    		<th><div>Archivo 2</div></th>

						</tr>

					</thead>

                    <tbody>

                    	<?php $count = 1;foreach($subjects as $row):?>

                        <tr>

							<td><?php echo $this->crud_model->get_type_name_by_id('class',$row['class_id']);?></td>

							<td><?php echo $row['name'];?></td>

							<td><?php echo $this->crud_model->get_type_name_by_id('teacher',$row['teacher_id']);?></td>

							<td><?php echo $row['nick_modulo'];?></td>



                            <?php if(!$row['file_name'] == null):?>

							<td>

                                <a href="<?php echo base_url().'uploads/document/'.$row['file_name']; ?>" target="_blank" class="btn btn-blue btn-icon icon-left">

                                    <i class="entypo-download"></i>

                                    <?php echo get_phrase('download');?>

                                </a> 

                            </td>



                            <?php else: ?>



                            <td>

                                <a href="<?php echo base_url().'uploads/document/'.$row['file_name']; ?>" disabled target="_blank" class="btn btn-blue btn-icon icon-left">

                                    <i class="entypo-download"></i>

                                    <?php echo get_phrase('download');?>

                                </a> 

                            </td>

                            <?php endif ?>



							<?php if(!$row['file_name_2'] == null){?>

							<td>

                                <a href="<?php echo base_url().'uploads/document/'.$row['file_name_2']; ?>" target="_blank" class="btn btn-blue btn-icon icon-left">

                                    <i class="entypo-download"></i>

                                    <?php echo get_phrase('download');?>

                                </a> 

                            </td>



                            <?php }else{ ?>



                            <td>

                                <a href="<?php echo base_url().'uploads/document/'.$row['file_name_2']; ?>" disabled target="_blank" class="btn btn-blue btn-icon icon-left">

                                    <i class="entypo-download"></i>

                                    <?php echo get_phrase('download');?>

                                </a> 

                            </td>

                            <?php } ?>

							

                        </tr>

                        <?php endforeach;?>

                    </tbody>

                </table>
			</div>
            <div class="tab-pane box" id="inscribir">
                <form id="form_inscripcion" action="<?php echo base_url().'index.php?admin/inscribir';?>" method="POST">
                    <input type="hidden" id="class_id" name="class_id" />
                    <input type="hidden" id="section_id" name="section_id" />
                    <input type="hidden" name="student_id" value="<?php echo $student_id ?>" />

                </form>
                <table class="table table-bordered datatable" id="table_export">
                	<thead>
                		<tr>
                    		<th><div><?php echo get_phrase('Clase');?></div></th>
<?php /*?>                    		<th><div><?php echo get_phrase('subject_name');?></div></th>
<?php */?><?php /*?>                            <th><div><?php echo get_phrase('Docente');?></div></th>
<?php */?>                            <?php /*?><th><div>nick modulo</div></th><?php */?>
                            <th><div>Sección</div></th>
                    		<th><div>Opciones</div></th>
						</tr>
					</thead>
                    <tbody>
                    	<?php foreach($courses as $row):?>
                        <tr>
                            <td><?php echo $row['name']; ?></td>
<?php /*?>                            <td><?php echo $row['sub_name']; ?></td>
<?php */?>                            <?php 
                            $seccions = $this->db->get_where('section' , array('class_id' => $row['class_id'] ))->result_array()
                            ?>
                            <td>
                                <select id="section">
                                <option>Seleccione</option>
                                
                                <?php foreach($seccions as $seccion): ?>
                                    <option value="<?php echo $seccion['section_id']; ?>">
                                    <?php echo $seccion['name']; ?>
                                    </option>
                                <?php endforeach;?>    
                                </select>
                            </td>
                            <td><a data-id="<?php echo $row['class_id']; ?>" href="#" class="form-control inscribir inline btn btn-primary"/>Inscribir</td>
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
                </form>    

            </div>
            <!----TABLE LISTING ENDS-->

            

            

			

            

		</div>

	</div>

</div>





<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      

<script type="text/javascript">



	jQuery(document).ready(function($)

	{
        //inscribimos
        $('.inscribir').click(function(e){
            e.preventDefault();
            $class = $(this).data('id');
            $('#class_id').val($class);
            $('form#form_inscripcion').submit();

        });

		//indicamos la seccion 
        $(document).on('change','#section',function(){
            $('#section_id').val($(this).val());
            
        });



		var datatable = $("#table_export").dataTable();

		

		$(".dataTables_wrapper select").select2({

			minimumResultsForSearch: -1

		});


        $('')

	});

		

</script>
</div>>