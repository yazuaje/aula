  <a class="bt_atras" href="index.php?admin">Volver al Inicio</a>

<style>
.sidebar-menu {
    display: none !important;
}
</style>
<?php echo form_open(base_url() . 'index.php?admin/manage_attendance_report'); ?>
<div class="row">

    <?php
    $query = $this->db->get('class');
    if ($query->num_rows() > 0):
        $class = $query->result_array();
        
        ?>

        <div class="col-md-5">
            <div class="form-group">
                <label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('class'); ?></label>
                <select class="form-control selectboxit" name="class_id" onchange="select_section(this.value)">
                    <option value=""><?php echo get_phrase('select_class'); ?></option>
                    <?php foreach ($class as $row): ?>
                    <option value="<?php echo $row['class_id']; ?>" ><?php echo $row['name']; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
    <?php endif; ?>


    <div id="section_holder">
        <div class="col-md-2">
            <div class="form-group">
                <label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('section'); ?></label>
                <select class="form-control selectboxit" name="section_id">
                    <option value=""><?php echo get_phrase('select_class_first') ?></option>

                </select>
            </div>
        </div>
    </div>
   
    <input type="hidden" name="operation" value="selection">
    <input type="hidden" name="year" value="<?php echo $running_year;?>">

	<div class="col-md-3" style="margin-top: 20px;">
		<button type="submit" class="btn btn-info"><?php echo get_phrase('show_report');?></button>
	</div>
</div>

<?php echo form_close(); ?>

<script type="text/javascript">
    function select_section(class_id) {

        $.ajax({
            url: '<?php echo base_url(); ?>index.php?admin/get_section/' + class_id,
            success: function (response)
            {

                jQuery('#section_holder').html(response);
            }
        });
    }
</script>



<div class="linea divisor1"></div>


<!-- ----------------FORMULARIO DE ASISTECIA----------------------- -->

<div class="row">
	<div class="col-md-12">
		<div id="dvData">
		<table class="table table-bordered">
			<thead>
			<?php
					$teacher = $this->db->get_where('section' , array('class_id' => $class_id , 'section_id' => $section_id))->row()->teacher_id;
					$subjects = $this->db->get_where('subject' , array('class_id' => $class_id , 'teacher_id' => $teacher))->result_array();
					$enrolls = $this->db->get_where('enroll' , array('class_id' => $class_id , 'section_id' => $section_id))->result_array();
			?>
			<tr>
				<td></td>
				<td style="text-align: center;" colspan="<?php echo count($subjects) ?>"><b><?php echo get_phrase('control_de_asistencia');?></b></td>
				<!--<td  style="text-align: center;" colspan="<?php echo count($subjects) ?>"><b><?php echo get_phrase('control_de_trabajos');?></b></td>-->
				<!--<td></td>-->

			</tr>
				<tr>
				<td style="text-align: center;"><?php echo get_phrase('Estudiantes| /Fechas-');?></td>
				<?php $i=1; foreach($subjects as $row): ?>
          <?php
            $class_routine_date = 	$this->db->get_where('class_routine' , array(
													                                  'class_id' => $class_id ,
															                              'section_id' => $section_id ,
																                            'subject_id' => $row['subject_id'],
																	                          'year' => $running_year
												                                  ))->row()->date; 
          ?>
					<td style="text-align: center;">
            <p><?php echo $row['nick_modulo'];?></p>
            <p><?php echo $class_routine_date;?></p>
          </td>
				<?php $i++; endforeach; ?>
				</tr>
			</thead>
			<tbody>
			<?php
				foreach($enrolls as $row):
			?>
				<tr>
					<td style="text-align: center;">
						<?php echo $this->db->get_where('student' , array('student_id' => $row['student_id']))->row()->name;?>
					</td>
          <!--<td></td>-->
				<?php
					foreach($subjects as $row2):
				?>
					<td style="text-align: center;">
						<?php
							  $class_routine_id = 	$this->db->get_where('class_routine' , array(
													                                  'class_id' => $row['class_id'] ,
															                              'section_id' => $row['section_id'] ,
																                            'subject_id' => $row2['subject_id'],
																	                          'year' => $running_year
												                                  ))->row()->class_routine_id;

							$status = 	$this->db->get_where('attendance' , array(
													                      'class_id' => $row['class_id'] ,
															                  'section_id' => $row['section_id'] ,
																                'student_id' => $row['student_id'],
																                'class_routine_id' => $class_routine_id,
																	              'year' => $running_year
												                      ))->row()->status;
                                              //  var_dump($row['class_id']);
                                              //  var_dump($row['section_id']);
                                              //  var_dump($row['student_id']);
                                              //  var_dump($row2['subject_id']);
                                              //  echo "class_routine ".$class_routine_id;
                                              // echo $status;
						?>
            <div class="row">
              <?php if($status == 1){ ?>
                <p class="text text-center"> Presente </p>
			  <?php } elseif ($status == 2){ ?>
                <p class="text text-center"> Ausente </p>
			  <?php } else{ ?>
				<p class="text text-center"> Indefinido </p>
			  <?php }?>
					</td>
				<?php endforeach;?>
				</tr>
			<?php endforeach;?>

			</tbody>
		</table>
		</div>
	</div>
</div>

<center>
	<a href="<?php echo base_url();?>index.php?admin/attendance_print_view/<?php echo $class_id;?>/<?php echo $section_id;?>"
				class="btn btn-primary" target="_blank">
				<?php echo get_phrase('inprimir pdf');?>
			</a>
			<a href="#"
				class="btn btn-primary" id="buttonExcel" target="_blank">
				<?php echo get_phrase('inprimir excel');?>
			</a>
		</center>

<!-- ----------------FORMULARIO DE ASISTECIA----------------------- -->

<script>
		$("#buttonExcel").click(function (e) 
		{
    		$(".table").table2excel({
					exclude: ".noExl",
					name: "Excel Document Name",
					filename: "asistencia",
					fileext: ".xls",
					exclude_img: true,
					exclude_links: true,
					exclude_inputs: true
				});
		});
</script>