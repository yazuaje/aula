  <a class="bt_atras" href="index.php?admin">Volver al Inicio</a>

<style>
.sidebar-menu {
    display: none !important;
}
</style>

<hr />
<div class="row">
	<div class="col-md-12">
		<?php echo form_open(base_url() . 'index.php?admin/tabulation_sheet');?>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label"><?php echo get_phrase('class');?></label>
					<select name="class_id" class="form-control selectboxit" onchange="select_section(this.value)">
                        <option value=""><?php echo get_phrase('select_a_class');?></option>
                        <?php
                        $classes = $this->db->get('class')->result_array();
                        foreach($classes as $row):
                        ?>
                            <option value="<?php echo $row['class_id'];?>"
                            	<?php if ($class_id == $row['class_id']) echo 'selected';?>>
                            		<?php echo $row['name'];?>
                            </option>
                        <?php
                        endforeach;
                        ?>
                    </select>
				</div>
			</div>
			<div class="col-md-4">
				<div id="section_holder">
					<div class="col-md-12">
				<div class="form-group">
					<label  style="margin-bottom: 5px;" class="control-label"><?php echo get_phrase('section');?></label>
					<select class="form-control selectboxit" name="section_id">
                            <option value=""><?php echo get_phrase('select_class_first') ?></option>

			</select>
				</div>
			</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<label class="control-label"><?php echo get_phrase('exam');?></label>
					<select name="exam_id" class="form-control selectboxit">
                        <option value=""><?php echo get_phrase('select_an_exam');?></option>
                        <?php
                        $exams = $this->db->get_where('exam' , array('year' => $running_year))->result_array();
                        foreach($exams as $row):
                        ?>
                            <option value="<?php echo $row['exam_id'];?>"
                            	<?php if ($exam_id == $row['exam_id']) echo 'selected';?>>
                            		<?php echo $row['name'];?>
                            </option>
                        <?php
                        endforeach;
                        ?>
                    </select>
				</div>
			</div>

			<input type="hidden" name="operation" value="selection">
			<div class="col-md-4" style="margin-top: 20px;">
				<button type="submit" class="btn btn-info"><?php echo get_phrase('view_tabulation_sheet');?></button>
			</div>
		<?php echo form_close();?>
	</div>
</div>

<script type="text/javascript">
    function select_section(class_id) {

        $.ajax({
            url: '<?php echo base_url(); ?>index.php?admin/get_section2/' + class_id,
            success:function (response)
            {

                jQuery('#section_holder').html(response);
            }
        });
    }
</script>

<?php if ($class_id != '' && $section_id != '' && $exam_id != ''):?>
<br>
<div class="row">
	<div class="col-md-4"></div>
	<div class="col-md-4" style="text-align: center;">
		<div class="tile-stats tile-gray">
		<div class="icon"><i class="entypo-docs"></i></div>
			<h3 style="color: #696969;">
				<?php
					$suma_total=0;
					$exam_name  = $this->db->get_where('exam' , array('exam_id' => $exam_id))->row()->name;
					$section_name = $this->db->get_where('section' , array('section_id' => $section_id))->row()->name;
					$class_name = $this->db->get_where('class' , array('class_id' => $class_id))->row()->name;
					echo get_phrase('tabulation_sheet');
				?>
			</h3>
			<h4 style="color: #696969;">
				<?php echo get_phrase('class') . ' ' . $class_name;?> : <?php echo $section_name;?> : <?php echo $exam_name;?>
			</h4>
		</div>
	</div>
	<div class="col-md-4"></div>
</div>


<hr />

<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered">
			<thead>
			<?php
					$teacher = $this->db->get_where('section' , array('class_id' => $class_id , 'section_id' => $section_id))->row()->teacher_id;
					$subjects = $this->db->get_where('subject' , array('class_id' => $class_id , 'teacher_id' => $teacher))->result_array();
					$enrolls = $this->db->get_where('enroll' , array('class_id' => $class_id , 'section_id' => $section_id))->result_array();
			?>
			<tr>
				<td></td>
				<td style="text-align: center;" colspan="<?php echo count($subjects) ?>"><b><?php echo get_phrase('control_de_asistencia');?></b></td>
				<td></td>
				<td  style="text-align: center;" colspan="<?php echo count($subjects) ?>"><b><?php echo get_phrase('control_de_trabajos');?></b></td>
				<td></td>
				<td></td>

			</tr>
				<tr>
<td style="text-align: center;">Codigo Estudiante</td>
				<td style="text-align: center;"><?php echo get_phrase('apellidos_y_nombres');?></td>
				<?php $i=1; foreach($subjects as $row): ?>
					<td style="text-align: center;"><?php echo $row['nick_modulo'];?></td>
				<?php $i++; endforeach; ?>
				<td style="text-align: center;"></td>
				<?php $i=1; foreach($subjects as $row): ?>
					<td style="text-align: center;"><?php echo $i;?></td>
				<?php $i++; endforeach; 
				$vista_final=$i;?>
				<td style="text-align: center;"><?php echo get_phrase('average_grade_point');?></td>
				</tr>
			</thead>
			<tbody>
			<?php

				foreach($enrolls as $row):
			?>
				<tr>
                <td style="text-align: center;">
						<?php echo $this->db->get_where('student' , array('student_id' => $row['student_id']))->row()->student_code;?>
					</td>
					<td style="text-align: center;">
						<?php echo $this->db->get_where('student' , array('student_id' => $row['student_id']))->row()->name;?>
					</td>
				<?php
					$total_marks = 0;
					$total_grade_point = 0;
					$total=0;
					foreach($subjects as $row2):
				?>
					<td style="text-align: center;">
						<?php
							$class_routine_id = 	$this->db->get_where('class_routine' , array(
													'class_id' => $row['class_id'] ,
															'section_id' => $row['section_id'] ,
																'subject_id' => $row2['subject_id'],
																	'year' => $running_year
												))->row()->class_routine_id;

							$status = 	$this->db->get_where('attendance' , array(
													'class_id' => $row['class_id'] ,
													'student_id' => $row['student_id'],
													'section_id' => $row['section_id'] ,
													'class_routine_id' => $class_routine_id,
													'year' => $running_year
												))->row()->status;

						// var_dump(' class_id:'.$row['class_id'].' -');
						// var_dump(' section_id:'.$row['section_id'].' -');
						// var_dump(' subject_id:'.$row2['subject_id'].' -');
						// var_dump(' class_routine_id:'.$class_routine_id.' -');

								if ($status==1){
									echo "Presente";
									$total=$total+1;
								}
								if ($status==2){
									echo "Ausente";
								}
								if(($status!=2)&&($status!=1)){
									echo "Indefinido";
								}


						?>
					</td>
				<?php endforeach;
			 
				?>
				<td></td>
				<?php
					$total_marks = 0;
					$total_grade_point = 0;
					foreach($subjects as $row2):
				?>
					<td style="text-align: center;">
						<?php
						// var_dump(' class_id:'.$row['class_id'].' -');
						// var_dump(' exam_id:'.$row['section_id'].' -');
						// var_dump(' subject_id:'.$row2['subject_id'].' -');
							$obtained_mark_query = 	$this->db->get_where('mark' , array(
													'class_id' => $row['class_id'] ,
													'section_id' => $section_id,
														'exam_id' => $exam_id ,
															'subject_id' => $row2['subject_id'] ,
																'student_id' => $row['student_id'],
																	'year' => $running_year
												));
							//	echo $row2['subject_id'].'-'.$row['student_id'].'-'.$exam_id.'';
							if ( $obtained_mark_query->num_rows() > 0) {
								$obtained_marks = $obtained_mark_query->row()->mark_obtained;
								// if ($obtained_marks==0)
								// 	echo "";
								// else
									echo $obtained_marks;
								if ($obtained_marks >= 0 && $obtained_marks != '') {
									$grade = $this->crud_model->get_grade($obtained_marks);
									$total_grade_point += $grade['grade_point'];
								}
								$total_marks += $obtained_marks;
							}
							else {
								echo "";
							}


						?>
					</td>
				<?php endforeach;?>
				<td style="text-align: center;">
					<?php
						$promedio=$total_marks / count($subjects);
						echo number_format($promedio,2,".","");
						if($promedio >10){ $suma_total=$suma_total+1;}					
?>
				</td>
				</tr>

			<?php 		 endforeach; 	?>
<tr>
				<td></td>

<td style="    background-color: #003471;
    color: white;
    text-align: center;
    font-weight: bold;">Total de Asistencia</td><?php 	foreach($subjects as $row2):

?><td style="    background-color: #FF0000;
    color: white;
    text-align: center;
    font-weight: bold;">		<?php
							$class_routine_id = 	$this->db->get_where('class_routine' , array(
													'class_id' => $row['class_id'] ,
															'section_id' => $row['section_id'] ,
																'subject_id' => $row2['subject_id'],
																	'year' => $running_year
												))->row()->class_routine_id;

							$status = 	$this->db->get_where('attendance' , array(
													'class_id' => $row['class_id'] ,
												 
													'section_id' => $row['section_id'] ,
													'class_routine_id' => $class_routine_id,
													'year' => $running_year,
													'status' => 1
												))->result_array();
print count($status);

						?></td>
<?php endforeach;?></tr>
			</tbody>
		</table>

		<center>
			<a href="<?php echo base_url();?>index.php?admin/tabulation_sheet_print_view/<?php echo $class_id;?>/<?php echo $section_id;?>/<?php echo $exam_id;?>"
				class="btn btn-primary" target="_blank">
				<?php echo get_phrase('print_tabulation_sheet');?>
			</a>
		</center>
	</div>
</div>
<?php endif;?>
