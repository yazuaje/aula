<?php
        $running_year = $this->db->get_where('settings' , array('type' => 'running_year'))->row()->description;
    
?>
<div id="print">
	<script src="assets/js/jquery-1.11.0.min.js"></script>
	<style type="text/css">
		td {
			padding: 5px;
		}
	</style>

	<center>
		<img src="uploads/logo.png" style="max-height : 60px;"><br>
		<h3 style="font-weight: 100;"><?php echo $system_name;?></h3>
		<?php echo get_phrase('tabulation_sheet');?><br>
		<?php echo get_phrase('class') . ' ' . $class_name;?><br>
		<?php echo $exam_name;?>


	</center>
<div class="row">
<div class="col-md-12">
		<table  style="width:100%; border-collapse:collapse;border: 1px solid #ccc; margin-top: 10px;">
			<thead>
			<?php
					$teacher = $this->db->get_where('section' , array('class_id' => $class_id , 'section_id' => $section_id))->row()->teacher_id;
					$subjects = $this->db->get_where('subject' , array('class_id' => $class_id , 'teacher_id' => $teacher))->result_array();
					$enrolls = $this->db->get_where('enroll' , array('class_id' => $class_id , 'section_id' => $section_id))->result_array();
			?>
			<tr>
				<td></td>
				<td style="text-align: center;" colspan="<?php echo count($subjects) ?>"><b><?php echo get_phrase('control_de_asistencia');?></b></td>
				<td></td>
				<td  style="text-align: center;" colspan="<?php echo count($subjects) ?>"><b><?php echo get_phrase('control_de_trabajos');?></b></td>
				<td></td>

			</tr>
				<tr>
<td style="text-align: center;">Codigo Estudiante</td>

				<td style="text-align: center;"><?php echo get_phrase('apellidos_y_nombres');?></td>
				<?php $i=1; foreach($subjects as $row): ?>
					<td style="text-align: center;"><?php echo $row['nick_modulo'];?></td>
				<?php $i++; endforeach; ?>
				<td style="text-align: center;"></td>
				<?php $i=1; foreach($subjects as $row): ?>
					<td style="text-align: center;"><?php echo $i;?></td>
				<?php $i++; endforeach; ?>
				<td style="text-align: center;"><?php echo get_phrase('average_grade_point');?></td>
				</tr>
			</thead>
			<tbody>
			<?php

				foreach($enrolls as $row):
			?>
				<tr>
                 <td style="text-align: center;">
						<?php echo $this->db->get_where('student' , array('student_id' => $row['student_id']))->row()->student_code;?>
					</td>
					<td style="text-align: center;">
						<?php echo $this->db->get_where('student' , array('student_id' => $row['student_id']))->row()->name;?>
					</td>
				<?php
					$total_marks = 0;
					$total_grade_point = 0;
					foreach($subjects as $row2):
				?>
					<td style="text-align: center;">
						<?php
							$class_routine_id = 	$this->db->get_where('class_routine' , array(
													'class_id' => $row['class_id'] ,
															'section_id' => $row['section_id'] ,
																'subject_id' => $row2['subject_id'],
																	'year' => $running_year
												))->row()->class_routine_id;

							$status = 	$this->db->get_where('attendance' , array(
													'class_id' => $row['class_id'] ,
													'student_id' => $row['student_id'],
													'section_id' => $row['section_id'] ,
													'class_routine_id' => $class_routine_id,
													'year' => $running_year
												))->row()->status;

						// var_dump(' class_id:'.$row['class_id'].' -');
						// var_dump(' section_id:'.$row['section_id'].' -');
						// var_dump(' subject_id:'.$row2['subject_id'].' -');
						// var_dump(' class_routine_id:'.$class_routine_id.' -');

								if ($status==1){
									echo "Presente";
								}
								if ($status==2){
									echo "Ausente";
								}
								if(($status!=2)&&($status!=1)){
									echo "Indefinido";
								}


						?>
					</td>
				<?php endforeach;?>
				<td></td>
				<?php
					$total_marks = 0;
					$total_grade_point = 0;
					foreach($subjects as $row2):
				?>
					<td style="text-align: center;">
						<?php
						// var_dump(' class_id:'.$row['class_id'].' -');
						// var_dump(' exam_id:'.$row['section_id'].' -');
						// var_dump(' subject_id:'.$row2['subject_id'].' -');
							$obtained_mark_query = 	$this->db->get_where('mark' , array(
													'class_id' => $row['class_id'] ,
														'exam_id' => $exam_id ,
															'subject_id' => $row2['subject_id'] ,
																'student_id' => $row['student_id'],
																	'year' => $running_year
												));
							//	echo $row2['subject_id'].'-'.$row['student_id'].'-'.$exam_id.'';
							if ( $obtained_mark_query->num_rows() > 0) {
								$obtained_marks = $obtained_mark_query->row()->mark_obtained;
								// if ($obtained_marks==0)
								// 	echo "";
								// else
									echo $obtained_marks;
								if ($obtained_marks >= 0 && $obtained_marks != '') {
									$grade = $this->crud_model->get_grade($obtained_marks);
									$total_grade_point += $grade['grade_point'];
								}
								$total_marks += $obtained_marks;
							}
							else {
								echo "";
							}


						?>
					</td>
				<?php endforeach;?>
				<td style="text-align: center;">
					<?php
						$promedio=$total_marks / count($subjects);
						echo number_format($promedio,2,".","");
					?>
				</td>
				</tr>

			<?php endforeach;?>
<tr><td style="    background-color: #003471;
    color: white;
    text-align: center;
    font-weight: bold;">Total de Asistencia</td><?php 	foreach($subjects as $row2):

?><td style="    background-color: #FF0000;
    color: white;
    text-align: center;
    font-weight: bold;">		<?php
							$class_routine_id = 	$this->db->get_where('class_routine' , array(
													'class_id' => $row['class_id'] ,
															'section_id' => $row['section_id'] ,
																'subject_id' => $row2['subject_id'],
																	'year' => $running_year
												))->row()->class_routine_id;

							$status = 	$this->db->get_where('attendance' , array(
													'class_id' => $row['class_id'] ,
												 
													'section_id' => $row['section_id'] ,
													'class_routine_id' => $class_routine_id,
													'year' => $running_year,
													'status' => 1
												))->result_array();
print count($status);

						?></td>
<?php endforeach;?></tr>
			</tbody>
		</table>
</div>
</div>



<script type="text/javascript">

	jQuery(document).ready(function($)
	{
		var elem = $('#print');
		PrintElem(elem);
		Popup(data);

	});

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data)
    {
        var mywindow = window.open('', 'my div', 'height=400,width=600');
        mywindow.document.write('<html><head><title></title>');
        //mywindow.document.write('<link rel="stylesheet" href="assets/css/print.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        //mywindow.document.write('<style>.print{border : 1px;}</style>');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print();
        mywindow.close();

        return true;
    }
</script>