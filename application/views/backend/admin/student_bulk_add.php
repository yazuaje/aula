  <a class="bt_atras" href="index.php?admin">Volver al Inicio</a>

<style>
.sidebar-menu {
    display: none !important;
}
.clear_123 {
    clear: both;
    padding-top: 27px;
}

.fileUpload.btn.btn-primary {
    background-color: #f3f3f3 !important;
    border: 0px !important;
    border-radius: 7px;
}

input.upload {
    color: #005aa6;
    font-weight: bold;
}



.fileUpload.btn.btn-primary span {
    color: #005aa6;
    font-weight: bold;
    font-size: 15px;
    padding-bottom:;
}
.important {
    background-color: #FFEB3B;
    color: black;
    font-size: 15px;
    padding: 16px 0px;
    margin-bottom: 17px;
}
a.btn.btn-success.22 {
    background-color: #F44336 !important;
    border: 0px !important;
}
</style>
<hr />
<?php  echo form_open(base_url() . 'index.php?admin/student_bulk_add/add_bulk_student' , 
			array('class' => 'form-inline validate', 'style' => 'text-align:center;','enctype'=>'multipart/form-data'));?>
<div class="row">
<div class="important"><strong>IMPORTANTE : </strong>Antes de importar alumnos debera llenar la informacion de <strong>Horario de Clase</strong> de la sección que desea registrar<br />
Descargar el <strong>Excel</strong> de ejemplo para subir alumnos en el orden correcto. <a class="btn btn-success 22" href="http://www.nuevohorizonte.edu.pe/aula/demo_import_alum/alumnos.xls" target="_blank">
			<i class="entypo-download"></i> Descargar Excel	</a> </div>
	<div class="col-md-2"></div>
	<div class="col-md-5">
		<div class="form_group">
			<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('class');?></label>
			<select name="class_id" id="class_id" class="form-control selectboxit" required="required"
				onchange="get_sections(this.value)"  data-validate="required"  data-message-required="<?php echo get_phrase('value_required');?>">
				<option value=""><?php echo get_phrase('select_class');?></option>
				<?php
				
					$classes = $this->db->get('class')->result_array();
					foreach($classes as $row):
				?>
				<option value="<?php echo $row['class_id'];?>"><?php echo $row['name'];?></option>
				<?php endforeach;?>
			</select>
		</div>
     
	</div>
    
	<div id="section_holder"></div>
    
    <div class="clear_123"></div>
       <div class="importar_cc">
        <div id="file_name" class="fileUpload btn btn-primary">
    <span>Importar Alumnos</span><br />

<input type="file" name="file_name"  class="form-control file2 inline btn btn-primary" data-label="<i class='entypo-upload'></i> Adjuntar" /></div>
        </div>
    
    
    
    
    
    
    
	<div class="col-md-2"></div>
</div>
<br><br>

<div id="bulk_add_form">
<div id="student_entry">
	<div class="row" style="margin-bottom:10px;">
<div class="form-group">
        	<input type="text" name="student_code[]" id="student_code" class="form-control student_code" style="width: 160px; margin-left: 5px;"
				placeholder="Codigo">
		</div>
		<div class="form-group">
			<input type="text" name="name[]" id="name" class="form-control name" style="width: 160px; margin-left: 5px;"
				placeholder="<?php echo get_phrase('name');?>" required>
		</div>
        
        
        	<div class="form-group">
			<input type="text" name="dni[]" id="dni" class="form-control dni" style="width: 160px; margin-left: 5px;"
				placeholder="DNI" required>
		</div>


	<div class="form-group">
	<select name="especialidad[]" id="especialidad" class="form-control especialidad" style="width: 120px; margin-left: 5px;">
				<option value="">Acreditación</option>
				<option value="inicial">Inicial</option>
				<option value="primaria">Primaria</option>
                <option value="secundaria">Secundaria</option>

			</select>
	  </div>

		<div class="form-group" style="display:none;">
			<input type="text" name="roll[]" id="roll" class="form-control roll" style="width: 60px; margin-left: 5px;"
				placeholder="<?php echo get_phrase('roll');?>">
		</div>

		<div class="form-group">
			<input type="email" name="email[]" id="email" class="form-control email" style="width: 160px; margin-left: 5px;"
				placeholder="Usuario" required>
		</div>
	
		<div class="form-group">
			<input type="password" name="password[]" id="password" class="form-control password" style="width: 150px; margin-left: 5px;"
				placeholder="<?php echo get_phrase('password');?>" required>
		</div>

		<div class="form-group">
			<input type="text" name="phone[]" id="phone" class="form-control phone" style="width: 140px; margin-left: 5px;"
				placeholder="<?php echo get_phrase('phone');?>">
		</div>

		<div class="form-group">
			<input type="text" name="address[]" id="address" class="form-control address" style="width: 240px; margin-left: 5px;"
				placeholder="Observación">
		</div>

		<div class="form-group">
			<select name="sex[]" id="sex" class="form-control sex" style="width: 110px; margin-left: 5px;">
				<option value=""><?php echo get_phrase('gender');?></option>
				<option value="male"><?php echo get_phrase('male');?></option>
				<option value="female"><?php echo get_phrase('female');?></option>
			</select>
		</div>

		<div class="form-group">
			<button type="button" class="btn btn-default" title="<?php echo get_phrase('remove');?>"
					onclick="deleteParentElement(this)" style="margin-left: 10px;">
        		<i class="entypo-trash" style="color: #696969;"></i>
        	</button>
		</div>

			
	</div>

</div>


<div id="student_entry_append"></div>
<br>

<div class="row">
	<center>
		<button type="button" class="btn btn-default" onclick="append_student_entry()">
			<i class="entypo-plus"></i> <?php echo get_phrase('add_a_row');?>
		</button>
	</center>
</div>

<br><br>

<div class="row">
	<center>
		<button type="submit" class="btn btn-success" id="submit_button">
			<i class="entypo-check"></i> <?php echo get_phrase('save_students');?>
		</button>
	</center>
</div>
</div>

<?php echo form_close();?>

<script type="text/javascript">

	var blank_student_entry ='';
	$(document).ready(function() {
		//$('#bulk_add_form').hide(); 
		blank_student_entry = $('#student_entry').html();

		for ($i = 0; $i<7;$i++) {
			$("#student_entry").append(blank_student_entry);
		}
		
	});

	function get_sections(class_id) {
		$.ajax({
            url: '<?php echo base_url();?>index.php?admin/get_sections/' + class_id ,
            success: function(response)
            {
                jQuery('#section_holder').html(response);
                jQuery('#bulk_add_form').show();
            }
        });
	}


	function append_student_entry()
	{
		$("#student_entry_append").append(blank_student_entry);
	}

	// REMOVING INVOICE ENTRY
	function deleteParentElement(n)
	{
		n.parentNode.parentNode.parentNode.removeChild(n.parentNode.parentNode);
	}

</script>


<script>
	$('#file_name').click(function (e) {
    // e.preventDefault();
	$(".name").each(function (index, element) {
    // element == this
    $(this).removeAttr('required');
	});
	$(".dni").each(function (index, element) {
    // element == this
    $(this).removeAttr('required');
	});
	$(".email").each(function (index, element) {
    // element == this
    $(this).removeAttr('required');
	});
	$(".password").each(function (index, element) {
    // element == this
    $(this).removeAttr('required');
	});
    
});
</script>
