<style>
a.btn.btn-blue.btn-icon.icon-left {
    background-color: red !important;
}

i.entypo-play {
    background-color: #ce0404 !important;
}
</style>
<script>
    // Sort by 3rd column first, and then 4th column
		   $(document).ready( function() {
		     $('#example').dataTable( {
		        "order": [[0,'desc']]
		      } );
		    } );
</script>

<a href="http://www.nuevohorizonte.edu.pe/tutorial-aula-virtual-docentes/" target="_blank"><div class="cuadr_tutorial">Tutorial Aula Virtual</div></a>
<!------- popup video ------->

	<div id="vidBox" class="id_video" style="display:none">
        <div id="videCont">
		<video  id="v1" loop controls>
            <source id="video_uno" src="" type="video/mp4">
        </video>
         </div>
        </div>
        

<!------- popup video ------->
<button onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/modal_diapositivas_add');"
    class="btn btn-primary pull-right">
        <?php echo get_phrase('add_study_material'); ?>
</button>
<div style="clear:both;"></div>
<br>
<table class="table table-bordered table-striped datatable" id="example">
    <thead>
        <tr>
            <th>#</th>
<?php /*?>            <th><?php echo get_phrase('date');?></th>
<?php */?>            <th><?php echo get_phrase('title');?></th>
            <th><?php echo get_phrase('description');?></th>
            <th><?php echo get_phrase('class');?></th>
            <th><?php echo get_phrase('Video');?></th>
            <th><?php echo get_phrase('options');?></th>
        </tr>
    </thead>

    <tbody>
        <?php
        $count = 1;
        foreach ($study_material_info as $row) { ?>
            <tr>
                <td><?php echo $count++; ?></td>
<?php /*?>                <td><?php echo date("d M, Y", $row['timestamp']); ?></td>
<?php */?>                <td><?php echo $row['title']?></td>
                <td><?php echo $row['description']?></td>
                <td>
                    <?php $name = $this->db->get_where('class' , array('class_id' => $row['class_id'] ))->row()->name;
                        echo $name;?>
                </td>
                <td>
                    <a   href="#" id="videos_<?php print $count;?>" onclick="video(<?php print $count;?>,'vidBox<?php print $count;?>')" datos="<?php echo base_url().'uploads/document/'.$row['file_name']; ?>" class="btn btn-blue btn-icon icon-left">
                        <i class="entypo-play"></i>
                        <?php echo get_phrase('Ver Video');?>
                    </a>
                </td>
                <td>
                    <a  onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/modal_diapositivas_edit/<?php echo $row['document_id']?>');"
                        class="btn btn-default btn-sm btn-icon icon-left">
                            <i class="entypo-pencil"></i>
                            <?php echo get_phrase('edit');?>
                    </a>
                    <a href="<?php echo base_url();?>index.php?admin/manage_diapositivas/delete/<?php echo $row['document_id']?>"
                        class="btn btn-danger btn-sm btn-icon icon-left" onclick="return confirm('Are you sure to delete?');">
                            <i class="entypo-cancel"></i>
                            <?php echo get_phrase('delete');?>
                    </a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<script type="text/javascript">
    jQuery(window).load(function ()
    {
        var $ = jQuery;

        $("#table-2").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>"
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });

        // Highlighted rows
        $("#table-2 tbody input[type=checkbox]").each(function (i, el)
        {
            var $this = $(el),
                    $p = $this.closest('tr');

            $(el).on('change', function ()
            {
                var is_checked = $this.is(':checked');

                $p[is_checked ? 'addClass' : 'removeClass']('highlight');
            });
        });

        // Replace Checboxes
        $(".pagination a").click(function (ev)
        {
            replaceCheckboxes();
        });
    });
</script>
    <script type="text/javascript">

function video(x,video){
 $(".id_video").attr('id','vidBox'+x);
var video_url= $("#videos_"+x+"").attr("datos");
 $("video").attr("src",video_url);


      $('#'+video).VideoPopUp({
                	backgroundColor: "#17212a",
                	opener: x,
                    maxweight: "640",
                    idvideo: "v1" 

            });
} 
    </script>
