<?php
    $class_name    =   $this->db->get_where('class' , array('class_id' => $class_id))->row()->name;
    $section_name  =   $this->db->get_where('section' , array('section_id' => $section_id))->row()->name;
    $system_name   =   $this->db->get_where('settings' , array('type'=>'system_name'))->row()->description;
    $running_year  =   $this->db->get_where('settings' , array('type'=>'running_year'))->row()->description;
?>


<div id="print">

	<script src="assets/js/jquery-1.11.0.min.js"></script>
	<style type="text/css">
		td {
			padding: 5px;
		}
	</style>

	<center>
		<img src="uploads/logo.png" style="max-height : 60px;"><br>
		<h3 style="font-weight: 100;"><?php echo $system_name;?></h3>
		<?php echo get_phrase('class_routine');?><br>
		<?php echo get_phrase('class') . ' ' . $class_name;?> : <?php echo get_phrase('section');?> <?php echo $section_name;?><br>
	</center>
    <br>
    
    
    

 <table cellpadding="0" cellspacing="0" border="0"  class="table table-bordered">
 <tr style="margin:5px; background-color: #444444;
    text-align: center;
    COLOR: white;
    font-weight: bold;" class="tab123">
<td class="tb_w" width="30%">Modulo</td>
<td class="tb_w" width="10%">Dia</td>
<td class="tb_w" width="10%">Fecha</td>
<td class="tb_w" width="10%">Hora</td>
<td class="tb_w" width="22%">Lugar</td>
</tr>
                    <tbody>
                        <?php 
                        for($d=1;$d<=7;$d++):
                        
                        if($d==1)$day='Domingo';
                        else if($d==2)$day='Lunes';
                        else if($d==3)$day='Martes';
                        else if($d==4)$day='Miercoles';
                        else if($d==5)$day='Jueves';
                        else if($d==6)$day='Viernes';
                        else if($d==7)$day='Sabado';
                        ?>
                        <tr class="gradeA">
<?php
$this->db->order_by("time_start", "asc");
$this->db->where('day' , $day);
$this->db->where('class_id' , $class_id);
$this->db->where('section_id' , $section_id);
$this->db->where('year' , $running_year);
$routines   =   $this->db->get('class_routine')->result_array();
foreach($routines as $row2):
?>
<td width="30%" style="    text-align: center;
    border: 1px solid #CCC;
    font-weight: bold;
    font-size: 13px !important;text-transform:capitalize;">
<?php echo $this->crud_model->get_subject_name_by_id($row2['subject_id']);?>
</td>
                        
                  <td style="    text-align: center;
    border: 1px solid #CCC;
    font-weight: bold;
    font-size: 12px !important;text-transform:capitalize;"  width="10%"><?php echo ($day);?>
                  </td>
                   
                  <td style="    text-align: center;
    border: 1px solid #CCC;
    font-weight: bold;
    font-size: 12px !important;text-transform:capitalize;"  width="10%"><?php echo ($row2['date']);?>
                  </td>
                   
                  <td style="    text-align: center;
    border: 1px solid #CCC;
    font-weight: bold;
    font-size: 12px !important;text-transform:capitalize;"  width="10%"><?php
                                        $changetime = 12;
                                            if ($row2['time_start_min'] == 0 && $row2['time_end_min'] == 0){
                                                echo $row2['time_start'].' : 00 AM<br>';
                                            echo $row2['time_end']-$changetime.' : 00 PM';
                                            }
                                            else{
                                         echo $row2['time_start'].':'.$row2['time_start_min'].' AM<br>'; 
                                          echo $row2['time_end']-$changetime.':'.$row2['time_end_min'].' PM';
                                        } ?>
                  </td>
                   
                  <td style="    text-align: center;
    border: 1px solid #CCC;
    font-weight: bold;
    font-size: 12px !important;text-transform:capitalize;"  width="22%"><?php echo ($row2['place']);?>
                  </td>      
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     
                          
                        </tr><?php endforeach;?>
                        <?php endfor;?>
                        
                    </tbody>
                </table>

</table>  
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
	

<br>

</div>


<script type="text/javascript">

	jQuery(document).ready(function($)
	{
		var elem = $('#print');
		PrintElem(elem);
		Popup(data);

	});

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
        var mywindow = window.open('', 'my div', 'height=400,width=600');
        mywindow.document.write('<html><head><title></title>');
        //mywindow.document.write('<link rel="stylesheet" href="assets/css/print.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        //mywindow.document.write('<style>.print{border : 1px;}</style>');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print();
        mywindow.close();

        return true;
    }
</script>