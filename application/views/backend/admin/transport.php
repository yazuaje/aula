<div class="izq_cuer_ord">

<style>
h3 {
    display: none;
}
</style>
<!-----------------FORMULARIO PARA ALERTA DE ARCHIVOS SUBIDOS-------------------->
<!--<h3 style="">
<i class="entypo-right-circled"></i> 
Alerta de Correo Electronico
</h3>-->
<!----CREATION FORM STARTS---->
<div class="tab-pane box" id="add" style="padding: 5px">
<div class="box-content">
<!--<?php echo form_open(base_url() . 'index.php?admin/transport/send_email' , array('class' => 'form-horizontal form-groups-bordered validate','target'=>'_top'));?>
<div class="padded">

<div class="form-group">
<label class="col-sm-3 control-label"><?php echo get_phrase('class');?></label>
<div class="col-sm-5">
<select name="class_id" class="form-control selectboxit" style="width:100%;">
<?php 
$classes = $this->db->get('class')->result_array();
foreach($classes as $row):
?>
<option value="<?php echo $row['class_id'];?>"
<?php if($row['class_id'] == $class_id) echo 'selected';?>>
<?php echo $row['name'];?>
</option>
<?php
endforeach;
?>
</select>

</div>
</div></div>
<div class="form-group">
<label class="col-sm-3 control-label">Contenido del Mensaje</label>
<div class="col-sm-5">
<textarea name="notice" id="ttt" rows="5" placeholder="Añadir Mensaje" class="form-control"></textarea>
</div>
</div>
<div class="form-group">
<div class="col-sm-offset-3 col-sm-5">
<button type="submit" class="btn btn-info">Enviar Correo</button>
</div>
</div>
</form>                -->
</div>                
</div>
<!----CREATION FORM ENDS-->

<!-----------------FORMULARIO PARA ALERTA DE ARCHIVOS SUBIDOS-------------------->









<!-----------------FORMULARIO PARA BUSCAR ALUMNOS-------------------->
<h3 style="">
<i class="entypo-right-circled 22"></i> 
Buscador de alumnos
</h3>
<!----CREATION FORM STARTS---->
<form id="search_form" class="form-horizontal form-groups-bordered validate">
<div class="padded">

<div class="form-group">
<label class="col-sm-3 control-label">Buscar por Dni</label>
<div class="col-sm-5">
<input class="form-control" id="dni" name="dni" type="text" />
</div>
</div></div>




<div class="form-group">
<div class="col-sm-offset-3 col-sm-5">
<button type="submit" class="btn btn-info">Buscar Alumno</button>
</div>
</div>




</form>

<script>
      $(function () {
        $('#search_form').on('submit', function (e) {

          e.preventDefault();

          $.ajax({
            type: 'post',
            url:  "<?php echo base_url() . 'index.php?admin/student_search'; ?>",
            data: $('#search_form').serialize(),
            success: function (response) {
                data = jQuery.parseJSON(response)
                if(!data.length == 0)
                {
                    $('#name').val(data[0].name)
                    $('#dni_2').val(data[0].dni)
                    $('#phone').val(data[0].phone)
                    $('#address').val(data[0].address)
                    $('#email').val(data[0].email)
                    $('#especialidad').val(data[0].especialidad)

                    $("#student_marksheet").attr("href", "<?php echo base_url().'index.php?admin/student_marksheet/';?>" + data[0].student_id )
                    $("#subject_student").attr("href", "<?php echo base_url().'index.php?admin/subject_student/';?>" + data[0].student_id )
                    $("#class_routine_student").attr("href", "<?php echo base_url().'index.php?admin/class_routine_student/';?>" + data[0].student_id )
                    $("#manage_attendance_report_student").attr("href", "<?php echo base_url().'index.php?admin/manage_attendance_report_student/';?>" + data[0].student_id );
				$("#datos_clase").load("<?php echo base_url().'index.php?admin/clases_id/';?>" + data[0].student_id);

                }
                else
                {
                    $('#name').val(' ')
                    $('#dni_2').val(' ')
                    $('#phone').val(' ')
                    $('#address').val(' ')
                    $('#email').val(' ')
                    $('#especialidad').val(' ')
					$("#datos_clase").html(' ');
                }
            }
          });

        });

      });
</script>
<!----CREATION FORM ENDS-------------------->


<!----DESPUES DE BUSCAR-------------------->

<form action="#" class="form-horizontal form-groups-bordered validate" target="_top" method="post" accept-charset="utf-8" novalidate="novalidate" style="margin-top:40px">
<div class="padded">
  
<div class="form-group">
<label class="col-sm-3 control-label">Apellidos y Nombre</label>
<div class="col-sm-5">
<input class="form-control" name="name" id="name" readonly value="" type="text" />
</div>
</div>


<div class="form-group">
<label class="col-sm-3 control-label">DNI</label>
<div class="col-sm-5">
<input class="form-control" name="dni" id="dni_2" readonly value="" type="text" />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label">ESPECIALIDAD</label>
<div class="col-sm-5">
<input class="form-control" name="especialidad" readonly id="especialidad" type="text" />
</div>
</div>

<div class="form-group">
<label class="col-sm-3 control-label">EMAIL</label>
<div class="col-sm-5">
<input class="form-control" name="email" readonly id="email" type="text" />
</div>
</div>


<div class="form-group">
<label class="col-sm-3 control-label">TELEFONO</label>
<div class="col-sm-5">
<input class="form-control" name="phone" readonly id="phone" type="text" />
</div>
</div>

<div class="form-group">
<label class="col-sm-3 control-label">DIRECCIÓN</label>
<div class="col-sm-5">
<input class="form-control" name="address" readonly id="address" type="text" />
</div>
</div>

<div class="row">
	<div class="col-md-12" id="datos_clase">
	</div>
</div>

<div class="cuerp_botones_11">
<a href="#" id="manage_attendance_report_student"><div class="cuerp_bt_elum ico_1_1">ASISTENCIA</div></a>
<a href="#" id="student_marksheet"><div class="cuerp_bt_elum ico_1_2">REPORTE DE NOTAS</div></a>
<a href="#" id="subject_student"><div class="cuerp_bt_elum ico_1_3">CURSOS</div></a>
<a href="#" id="class_routine_student"><div class="cuerp_bt_elum ico_1_4">HORARIOS DE CLASE</div></a>
<a href="#" class="demo-2" id="class_routine_student"><div class="cuerp_bt_elum ico_1_4">POLITICA</div></a>
<div class="container">


            <div id="popup1">
         <iframe src="http://www.nuevohorizonte.edu.pe/descargar_reglamento/REGLAMENTO_INTERNO_DE_CAPACITACI%C3%93N.pdf" width="100%" height="4700px" scrolling="no"></iframe>

            </div>
</div>

</div>









</div>
</form>

<!----DESPUES DE BUSCAR-------------------->
<!-----------------FORMULARIO PARA BUSCAR ALUMNOS-------------------->







<!-----------------FORMULARIO PARA BUSCAR ALUMNOS------------------
<h3 style="">
<i class="entypo-right-circled"></i> 
Buscador de alumnos
</h3>
<!----CREATION FORM STARTS---->
<!--<form action="#" class="form-horizontal form-groups-bordered validate" target="_top" method="post" accept-charset="utf-8" novalidate="novalidate">
<div class="padded">

<div class="form-group">
<label class="col-sm-3 control-label">Buscar por Dni</label>
<div class="col-sm-5">
<input class="form-control" name="" type="text" />
</div>
</div></div>




<div class="form-group">
<div class="col-sm-offset-3 col-sm-5">
<button type="submit" class="btn btn-info">Buscar Alumno</button>
</div>
</div>




</form>

<form action="#" class="form-horizontal form-groups-bordered validate" target="_top" method="post" accept-charset="utf-8" novalidate="novalidate">
<div class="padded">

<div class="form-group">
<label class="col-sm-3 control-label">Apellidos y Nombre</label>
<div class="col-sm-5">
<input class="form-control" name="" type="text" />
</div>
</div>


<div class="form-group">
<label class="col-sm-3 control-label">DNI</label>
<div class="col-sm-5">
<input class="form-control" name="" type="text" />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label">ESPECIALIDAD</label>
<div class="col-sm-5">
<input class="form-control" name="" type="text" />
</div>
</div>

<div class="form-group">
<label class="col-sm-3 control-label">EMAIL</label>
<div class="col-sm-5">
<input class="form-control" name="" type="text" />
</div>
</div>


<div class="form-group">
<label class="col-sm-3 control-label">TELEFONO</label>
<div class="col-sm-5">
<input class="form-control" name="" type="text" />
</div>
</div>

<div class="form-group">
<label class="col-sm-3 control-label">DIRECCIÓN</label>
<div class="col-sm-5">
<input class="form-control" name="" type="text" />
</div>
</div>


<div class="cuerp_botones_11">
<a href="#"><div class="cuerp_bt_elum ico_1_1">ASISTENCIA</div></a>
<a href="#"><div class="cuerp_bt_elum ico_1_2">REPORTE DE NOTAS</div></a>
<a href="#"><div class="cuerp_bt_elum ico_1_3">CURSOS</div></a>
<a href="#"><div class="cuerp_bt_elum ico_1_4">HORARIOS DE CLASE</div></a>


</div>









</div>
</form>-->

<!----DESPUES DE BUSCAR-------------------->
<!-----------------FORMULARIO PARA BUSCAR ALUMNOS-------------------->


































<?php /*?><hr />
<div class="row">
	<div class="col-md-12">
    
    	<!---CONTROL TABS START-->
		<ul class="nav nav-tabs bordered">
			<li class="active">
            	<a href="#list" data-toggle="tab"><i class="entypo-menu"></i> 
					<?php echo get_phrase('transport_list');?>
                    	</a></li>
			<li>
            	<a href="#add" data-toggle="tab"><i class="entypo-plus-circled"></i>
					<?php echo get_phrase('add_transport');?>
                    	</a></li>
		</ul>
    	<!---CONTROL TABS END-->
        
	
		<div class="tab-content">
        <br>
            <!--TABLE LISTING STARTS-->
            <div class="tab-pane box active" id="list">
                <table class="table table-bordered datatable" id="table_export">
                	<thead>
                		<tr>
                    		<th><div><?php echo get_phrase('route_name');?></div></th>
                    		<th><div><?php echo get_phrase('number_of_vehicle');?></div></th>
                    		<th><div><?php echo get_phrase('description');?></div></th>
                    		<th><div><?php echo get_phrase('route_fare');?></div></th>
                    		<th><div><?php echo get_phrase('options');?></div></th>
						</tr>
					</thead>
                    <tbody>
                    	<?php $count = 1;foreach($transports as $row):?>
                        <tr>
							<td><?php echo $row['route_name'];?></td>
							<td><?php echo $row['number_of_vehicle'];?></td>
							<td><?php echo $row['description'];?></td>
							<td><?php echo $row['route_fare'];?></td>
							<td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    Action <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                    <!-- STUDENTS IN THE TRANSPORT -->
                                    <li>
                                        <a href="#" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/modal_transport_student/<?php echo $row['transport_id'];?>');">
                                            <i class="entypo-users"></i>
                                                <?php echo get_phrase('students');?>
                                            </a>
                                                    </li>
                                    <li class="divider"></li>
                                    
                                    <!-- EDITING LINK -->
                                    <li>
                                        <a href="#" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/modal_edit_transport/<?php echo $row['transport_id'];?>');">
                                            <i class="entypo-pencil"></i>
                                                <?php echo get_phrase('edit');?>
                                            </a>
                                                    </li>
                                    <li class="divider"></li>
                                    
                                    <!-- DELETION LINK -->
                                    <li>
                                        <a href="#" onclick="confirm_modal('<?php echo base_url();?>index.php?admin/transport/delete/<?php echo $row['transport_id'];?>');">
                                            <i class="entypo-trash"></i>
                                                <?php echo get_phrase('delete');?>
                                            </a>
                                                    </li>
                                </ul>
                            </div>
        					</td>
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
			</div>
            <!--TABLE LISTING ENDS-->
            
            
			<!--CREATION FORM STARTS-->
			<div class="tab-pane box" id="add" style="padding: 5px">
                <div class="box-content">
                	<?php echo form_open(base_url() . 'index.php?admin/transport/create' , array('class' => 'form-horizontal form-groups-bordered validate','target'=>'_top'));?>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('route_name');?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="route_name"
                                        data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('number_of_vehicle');?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="number_of_vehicle"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('description');?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="description"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('route_fare');?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="route_fare"/>
                                </div>
                            </div>
                        <div class="form-group">
                              <div class="col-sm-offset-3 col-sm-5">
                                  <button type="submit" class="btn btn-info"><?php echo get_phrase('add_transport');?></button>
                              </div>
							</div>
                    </form>                
                </div>                
			</div>
			<!--CREATION FORM ENDS-->
            
		</div>
	</div>
</div><?php */?>
</div>
  <link href="assets/js/popup/jquery.simple-popup.min.css" rel="stylesheet" type="text/css">

<script src="assets/js/popup/jquery.simple-popup.min.js"></script>
<script>
$(document).ready(function() {
  $("a.demo-1").simplePopup();
  $("a.demo-2").simplePopup({ type: "html", htmlSelector: "#popup1" });
});
</script>