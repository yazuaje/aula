  <a class="bt_atras" href="index.php?admin">Volver al Inicio</a>

<style>
.sidebar-menu {
    display: none !important;
}
td.sorting_1 {
    font-size: 13px !important;
}

p.text.text-justify.small {
    font-size: 15px !important;
    color: #00a651;
}

p.text.text-center.small {
    font-size: 15px !important;
    color: red !important;
}

input.form-control {
    width: 22px !important;
}

td.sorting_1 {
    font-weight: bold !important;
}
input.form-control.input-sm {
}
</style>
<?php echo form_open(base_url() . 'index.php?admin/manage_attendance3');?>
<div class="row izq_cuer_ord">

	<div class="col-md-5">
		<div class="form-group">
		<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('class');?></label>
			<select name="class_id" class="form-control selectboxit" onchange="select_section(this.value)">
				<option value=""><?php echo get_phrase('select_class');?></option>
				<?php
					$classes = $this->db->get('class')->result_array();
					foreach($classes as $row):
                                            
				?>
                                
				<option value="<?php echo $row['class_id'];?>"
					><?php echo $row['name'];?></option>
                                
				<?php endforeach;?>
			</select>
		</div>
	</div>

	
    <div id="section_holder">
	<div class="col-md-2">
		<div class="form-group">
		<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('section');?></label>
			<select class="form-control selectboxit" name="section_id">
                            <option value=""><?php echo get_phrase('select_class_first') ?></option>
				
			</select>
		</div>
	</div>
    </div>
	
        <!--<div class="col-md-3">
		<div class="form-group">
		<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('date');?></label>
			<input type="text" class="form-control datepicker" name="timestamp" data-format="dd-mm-yyyy"
				value="<?php echo date("d-m-Y");?>"/>
		</div>
	</div>-->
	<input type="hidden" name="year" value="<?php echo $running_year;?>">

	<div class="col-md-3" style="margin-top: 20px;">
		<button type="submit" class="btn btn-info"><?php echo get_phrase('manage_attendance');?></button>
	</div>

</div>
<?php echo form_close();?>

<script type="text/javascript">
    function select_section(class_id) {

        $.ajax({
            url: '<?php echo base_url(); ?>index.php?admin/get_section/' + class_id,
            success:function (response)
            {

                jQuery('#section_holder').html(response);
            }
        });
    }
</script>

<div class="linea divisor1"></div>


<!-- ----------------FORMULARIO DE ASISTECIA----------------------- -->

<div class="row">
	<div class="col-md-12">
		<?php
			$class_name = $this->db->get_where('class' , array('class_id' => $class_id ))->row()->name;
			$section_name = $this->db->get_where('section' , array('class_id' => $class_id , 'section_id' => $section_id))->row()->name;
		 ?>
		<p class='text text-center'> <b>Clase:</b> <?php echo $class_name; ?></p>
		<p class='text text-center'> <b>Seccion:</b> <?php echo $section_name; ?></p>
		<table class="table table-bordered table-striped datatable"  id="example">
			<thead>
			<?php
					$teacher = $this->db->get_where('section' , array('class_id' => $class_id , 'section_id' => $section_id))->row()->teacher_id;
					$subjects = $this->db->get_where('subject' , array('class_id' => $class_id , 'teacher_id' => $teacher))->result_array();
					$enrolls = $this->db->get_where('enroll' , array('class_id' => $class_id , 'section_id' => $section_id))->result_array();
			?>
			<tr>
				<td></td>
				<td style="text-align: center;" colspan="<?php echo count($subjects) ?>"><b><?php echo get_phrase('control_de_asistencia');?></b></td>
				<!--<td  style="text-align: center;" colspan="<?php echo count($subjects) ?>"><b><?php echo get_phrase('control_de_trabajos');?></b></td>-->
				<!--<td></td>-->

			</tr>
				<tr>
				<td style="text-align: center;"><?php echo get_phrase('Estudiantes| /Fechas-');?></td>
				<?php $i=1; foreach($subjects as $row): ?>
          <?php
            $class_routine_date = 	$this->db->get_where('class_routine' , array(
													                                  'class_id' => $class_id ,
															                              'section_id' => $section_id ,
																                            'subject_id' => $row['subject_id'],
																	                          'year' => $running_year
												                                  ))->row()->date; 
          ?>
					<td style="text-align: center;">
            <p><?php echo $row['nick_modulo'];?></p>
            <p><?php echo $class_routine_date;?></p>
          </td>
				<?php $i++; endforeach; ?>
				</tr>
			</thead>
			<tbody>
			<?php
				foreach($enrolls as $row):
			?>
				<tr>
					<td style="text-align: center;">
						<?php echo $this->db->get_where('student' , array('student_id' => $row['student_id']))->row()->name;?>
					</td>
          <!--<td></td>-->
				<?php
					foreach($subjects as $row2):
				?>
					<td style="text-align: center;">
						<?php
							  $class_routine_id = 	$this->db->get_where('class_routine' , array(
													                                  'class_id' => $row['class_id'] ,
															                              'section_id' => $row['section_id'] ,
																                            'subject_id' => $row2['subject_id'],
																	                          'year' => $running_year
												                                  ))->row()->class_routine_id;

							$status = 	$this->db->get_where('attendance' , array(
													                      'class_id' => $row['class_id'] ,
															                  'section_id' => $row['section_id'] ,
																                'student_id' => $row['student_id'],
																                'class_routine_id' => $class_routine_id,
																	              'year' => $running_year
												                      ))->row()->status;
                                              //  var_dump($row['class_id']);
                                              //  var_dump($row['section_id']);
                                              //  var_dump($row['student_id']);
                                              //  var_dump($row2['subject_id']);
                                              //  echo "class_routine ".$class_routine_id;
                                              // echo $status;
						?>
            <div class="row">
              <?php if($status == 1){ ?>
                <div class="col-md-6">
                  <input type="radio" class="form-control" value="1" data-ts-year="<?php echo $running_year; ?>" data-ts-classRoutineId="<?php echo $class_routine_id; ?>" data-ts-sectionId="<?php echo $row['section_id']; ?>" data-ts-classId="<?php echo $row['class_id']; ?>" data-ts-studentId="<?php echo $row['student_id'];?>" <?php echo 'checked'; ?>   name="asistencia-<?php echo $row['student_id'];?>-<?php echo $row2['subject_id']; ?>">
                  <p class="text text-justify small">presente</p>
                </div>
                <div class="col-md-6">
                  <input type="radio" class="form-control" value="2" data-ts-year="<?php echo $running_year; ?>" data-ts-classRoutineId="<?php echo $class_routine_id; ?>" data-ts-sectionId="<?php echo $row['section_id']; ?>" data-ts-classId="<?php echo $row['class_id']; ?>" data-ts-studentId="<?php echo $row['student_id'];?>" name="asistencia-<?php echo $row['student_id'];?>-<?php echo $row2['subject_id']; ?>">
                  <p class="text text-center small">ausente</p>
                </div>
              </div>
							<?php } elseif ($status == 2) {?>
                <div class="col-md-6">
                  <input type="radio" class="form-control" value="1" data-ts-year="<?php echo $running_year; ?>" data-ts-classRoutineId="<?php echo $class_routine_id; ?>" data-ts-sectionId="<?php echo $row['section_id']; ?>" data-ts-classId="<?php echo $row['class_id']; ?>" data-ts-studentId="<?php echo $row['student_id'];?>"  name="asistencia-<?php echo $row['student_id'];?>-<?php echo $row2['subject_id']; ?>">
                  <p class="text text-justify small">presente</p>
                </div>
                <div class="col-md-6">
                  <input type="radio" class="form-control" value="2" data-ts-year="<?php echo $running_year; ?>" data-ts-classRoutineId="<?php echo $class_routine_id; ?>" data-ts-sectionId="<?php echo $row['section_id']; ?>" data-ts-classId="<?php echo $row['class_id']; ?>" data-ts-studentId="<?php echo $row['student_id'];?>" <?php echo 'checked'; ?> name="asistencia-<?php echo $row['student_id'];?>-<?php echo $row2['subject_id']; ?>">
                  <p class="text text-center small">ausente</p>
                </div>
              </div>
							<?php } else{ ?>
								<div class="col-md-6">
                  <input type="radio" class="form-control" value="1" data-ts-year="<?php echo $running_year; ?>" data-ts-classRoutineId="<?php echo $class_routine_id; ?>" data-ts-sectionId="<?php echo $row['section_id']; ?>" data-ts-classId="<?php echo $row['class_id']; ?>" data-ts-studentId="<?php echo $row['student_id'];?>"  name="asistencia-<?php echo $row['student_id'];?>-<?php echo $row2['subject_id']; ?>">
                  <p class="text text-justify small">presente</p>
                </div>
                <div class="col-md-6">
                  <input type="radio" class="form-control" value="2" data-ts-year="<?php echo $running_year; ?>" data-ts-classRoutineId="<?php echo $class_routine_id; ?>" data-ts-sectionId="<?php echo $row['section_id']; ?>" data-ts-classId="<?php echo $row['class_id']; ?>" data-ts-studentId="<?php echo $row['student_id'];?>" name="asistencia-<?php echo $row['student_id'];?>-<?php echo $row2['subject_id']; ?>">
                  <p class="text text-center small">ausente</p>
                </div>
              </div>
							<?php } ?>
					</td>
				<?php endforeach;?>
				</tr>
			<?php endforeach;?>

			</tbody>
		</table>
	</div>
</div>

<!-- ----------------FORMULARIO DE ASISTECIA----------------------- -->

<script>
  $('input[type=radio]').click(function (e) 
  {
    $.ajax({
    type: "post",
    url: "<?php echo base_url() . 'index.php?admin/update_status'; ?>",
    data: { class_routine_id: $(this).attr("data-ts-classRoutineId"), section_id : $(this).attr("data-ts-sectionId"), class_id: $(this).attr("data-ts-classId"), student_id: $(this).attr("data-ts-studentId"), year: $(this).attr("data-ts-year"), status: $(this).val()} ,
    success: function (response) {
        
    }
    });
});
</script>











<script>
    // Sort by 3rd column first, and then 4th column
		   $(document).ready( function() {
		     $('#example').dataTable( {
"lengthMenu": [ [-1], [ "FULL"] ],
 "order": [[0,'asc']]				
		      } );
		    } );
</script>






<script type="text/javascript">
    jQuery(window).load(function ()
    {
        var $ = jQuery;

        $("#table-2").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>"
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });

        // Highlighted rows
        $("#table-2 tbody input[type=checkbox]").each(function (i, el)
        {
            var $this = $(el),
                    $p = $this.closest('tr');

            $(el).on('change', function ()
            {
                var is_checked = $this.is(':checked');

                $p[is_checked ? 'addClass' : 'removeClass']('highlight');
            });
        });

        // Replace Checboxes
        $(".pagination a").click(function (ev)
        {
            replaceCheckboxes();
        });
    });
</script>
   