<div class="izq_cuer_ord">

<h3 style="">
<i class="entypo-right-circled"></i> 
Alerta de Correo Electronico
</h3>
<!----CREATION FORM STARTS---->
<div class="tab-pane box" id="add" style="padding: 5px">
<div class="box-content">
<?php echo form_open(base_url() . 'index.php?admin/email_notification/send_email' , array('class' => 'form-horizontal form-groups-bordered validate','target'=>'_top'));?>
<div class="padded">

<div class="form-group">
<label class="col-sm-3 control-label"><?php echo get_phrase('class');?></label>
<div class="col-sm-5">
<select name="class_id" class="form-control selectboxit" style="width:100%;">
<?php 
$classes = $this->db->get('class')->result_array();
foreach($classes as $row):
?>
<option value="<?php echo $row['class_id'];?>"
<?php if($row['class_id'] == $class_id) echo 'selected';?>>
<?php echo $row['name'];?>
</option>
<?php
endforeach;
?>
</select>

</div>
</div></div>
<div class="form-group">
<label class="col-sm-3 control-label">Contenido del Mensaje</label>
<div class="col-sm-5">
<textarea name="notice" id="ttt" rows="5" placeholder="Añadir Mensaje" class="form-control"></textarea>
</div>
</div>
<div class="form-group">
<div class="col-sm-offset-3 col-sm-5">
<button type="submit" class="btn btn-info">Enviar Correo</button>
</div>
</div>
</form>                
</div>                
</div>

</div>