<?php
        $running_year = $this->db->get_where('settings' , array('type' => 'running_year'))->row()->description;
        $class_name = $this->db->get_where('class' , array('class_id' => $class_id))->row()->name;
        $section_name = $this->db->get_where('section' , array('section_id' => $section_id))->row()->name;


?>
<div id="print">
	<script src="assets/js/jquery-1.11.0.min.js"></script>
	<style type="text/css">
		td {
			padding: 5px;
		}
	</style>

	<center>
		<img src="uploads/logo.png" style="max-height : 60px;"><br>
		<h3 style="font-weight: 100;"><?php echo $system_name;?></h3>
		<?php echo get_phrase('hoja de asistencia');?><br>
		<?php echo get_phrase('class') . ' ' . $class_name;?><br>
        <?php echo get_phrase('seccion') . ': ' . $section_name;?><br>
	</center>


	<table style="width:100%; border-collapse:collapse;border: 1px solid #ccc; margin-top: 10px;" border="1">
			<thead>
			<?php
					$teacher = $this->db->get_where('section' , array('class_id' => $class_id , 'section_id' => $section_id))->row()->teacher_id;
					$subjects = $this->db->get_where('subject' , array('class_id' => $class_id , 'teacher_id' => $teacher))->result_array();
					$enrolls = $this->db->get_where('enroll' , array('class_id' => $class_id , 'section_id' => $section_id))->result_array();
			?>
			<tr>
				<td></td>
				<td style="text-align: center;" colspan="<?php echo count($subjects) ?>"><b><?php echo get_phrase('control_de_asistencia');?></b></td>
				<!--<td  style="text-align: center;" colspan="<?php echo count($subjects) ?>"><b><?php echo get_phrase('control_de_trabajos');?></b></td>-->
				<!--<td></td>-->

			</tr>
				<tr>
				<td style="text-align: center;"><?php echo get_phrase('Estudiantes| /Fechas-');?></td>
				<?php $i=1; foreach($subjects as $row): ?>
          <?php
            $class_routine_date = 	$this->db->get_where('class_routine' , array(
													                                  'class_id' => $class_id ,
															                              'section_id' => $section_id ,
																                            'subject_id' => $row['subject_id'],
																	                          'year' => $running_year
												                                  ))->row()->date; 
          ?>
					<td style="text-align: center;">
            <p><?php echo $row['nick_modulo'];?></p>
            <p><?php echo $class_routine_date;?></p>
          </td>
				<?php $i++; endforeach; ?>
				</tr>
			</thead>
			<tbody>
			<?php
				foreach($enrolls as $row):
			?>
				<tr>
					<td style="text-align: center;">
						<?php echo $this->db->get_where('student' , array('student_id' => $row['student_id']))->row()->name;?>
					</td>
          <!--<td></td>-->
				<?php
					foreach($subjects as $row2):
				?>
					<td style="text-align: center;">
						<?php
							  $class_routine_id = 	$this->db->get_where('class_routine' , array(
													                                  'class_id' => $row['class_id'] ,
															                              'section_id' => $row['section_id'] ,
																                            'subject_id' => $row2['subject_id'],
																	                          'year' => $running_year
												                                  ))->row()->class_routine_id;

							$status = 	$this->db->get_where('attendance' , array(
													                      'class_id' => $row['class_id'] ,
															                  'section_id' => $row['section_id'] ,
																                'student_id' => $row['student_id'],
																                'class_routine_id' => $class_routine_id,
																	              'year' => $running_year
												                      ))->row()->status;
                                              //  var_dump($row['class_id']);
                                              //  var_dump($row['section_id']);
                                              //  var_dump($row['student_id']);
                                              //  var_dump($row2['subject_id']);
                                              //  echo "class_routine ".$class_routine_id;
                                              // echo $status;
						?>
            <div class="row">
              <?php if($status == 1){ ?>
                <p class="text text-center"> Presente </p>
			  <?php } elseif ($status == 2){ ?>
                <p class="text text-center"> Ausente </p>
			  <?php } else{ ?>
				<p class="text text-center"> Indefinido </p>
			  <?php }?>
					</td>
				<?php endforeach;?>
				</tr>
			<?php endforeach;?>

			</tbody>
		</table>
</div>



<script type="text/javascript">

	jQuery(document).ready(function($)
	{
		var elem = $('#print');
		PrintElem(elem);
		Popup(data);

	});

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data)
    {
        var mywindow = window.open('', 'my div', 'height=400,width=600');
        mywindow.document.write('<html><head><title></title>');
        //mywindow.document.write('<link rel="stylesheet" href="assets/css/print.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        //mywindow.document.write('<style>.print{border : 1px;}</style>');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print();
        mywindow.close();

        return true;
    }
</script>