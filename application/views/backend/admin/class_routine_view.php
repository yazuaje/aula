<div class="izq_cuer_ord">

<style>
tr.tab123 {
    background-color: #d8d8d8;
    color: white !important;
    text-align: center;
    font-size: 21px;
    font-weight: bold;
}
table.tb147 tr td {
    text-align: center;
    color: #4a4a4a !important;
    font-weight: bold;
    padding: 10px;
    font-size: 15px;
    border: 1px solid #ccc;
}
a.btn.btn-primary.btn-xs.pull-right {
    background-color: #4faeff;
    padding: 3px;
    padding-left: 10px;
    padding-right: 11px;
}
td.tb_w {
    color: #2b2b2b !important;
    font-size: 16px;
}
.align-center td{
text-align: center;
}
.align-center td:last-child{
border-right: none;
    }
	.clase_text{ text-transform:capitalize;}
</style>
<hr />
<a href="<?php echo base_url();?>index.php?admin/class_routine_add"
    class="btn btn-primary pull-right">
        <i class="entypo-plus-circled"></i>
        <?php echo get_phrase('add_class_routine');?>
    </a> 
<br><br><br>

<?php
	$query = $this->db->get_where('section' , array('class_id' => $class_id));
	if($query->num_rows() > 0):
		$sections = $query->result_array();
	foreach($sections as $row):
?>
<div class="row">
	
    <div class="col-md-12">

        <div class="panel panel-default" data-collapsed="0">
            <div class="panel-heading" >
                <div class="panel-title" style="font-size: 16px; color: white; text-align: center;">
                
                
                
                    <?php echo get_phrase('class');?> - <?php echo $this->db->get_where('class' , array('class_id' => $class_id))->row()->name;?> : 
                    
                    <?php echo get_phrase('section');?> - <?php echo $this->db->get_where('section' , array('section_id' => $row['section_id']))->row()->name;?>
                    
                    <a href="<?php echo base_url();?>index.php?admin/class_routine_print_view/<?php echo $class_id;?>/<?php echo $row['section_id'];?>" 
                        class="btn btn-primary btn-xs pull-right" target="_blank">
                            <i class="entypo-print"></i> <?php echo get_phrase('print'); ?>
                    </a>
                    </br>
                    <?php echo get_phrase('Supervisor Academico');?> - <?php echo $this->db->get_where('section' , array('section_id' => $row['section_id']))->row()->nick_name;?>
                </div>
            </div>
<!-- TABLA PARA MODIFICAR -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tb147">

<!--<tr>
<td>Marco Legal, Organizacional 
y Administrativo de la Gesti��n 
P��blica Municipal. </td>
<td>Lunes </td>
<td>25 de febrero</td>
<td>8:00 AM </br>
12:00  PM</td>
<td>Universidad Naciona de Trujillo</td>
<td>Boton Editar / eliminar</td>
</tr>-->
</table>
<!-- TABLA PARA MODIFICAR -->
            <div class="panel-body1">
                
                <table cellpadding="0" cellspacing="0" border="0"  class="table table-bordered align-center">
                <tr class="tab123">
<td class="tb_w" width="30%">Modulo</td>
<td class="tb_w" width="10%">Dia</td>
<td class="tb_w" width="15%">Fecha</td>
<td class="tb_w" width="10%">Hora</td>
<td class="tb_w" width="22%">Lugar</td>
<td  class="tb_w" width="30%">Editar</td>

</tr>
                    <tbody>
                        <?php 
                        for($d=1;$d<=7;$d++):
                        
                        if($d==2)$day='Domingo';
                        else if($d==3)$day='Lunes';
                        else if($d==4)$day='Martes';
                        else if($d==5)$day='Miercoles';
                        else if($d==6)$day='-';
                        else if($d==7)$day='Viernes';
                        else if($d==1)$day='Sabado';
                        ?>
                        
                        <tr class="gradeA">
                            <!-- <td width="100"><?php //echo strtoupper($day);?></td> -->
                            
                                <?php
                                $this->db->order_by("time_start", "asc");
								//$this->db->order_by("class_routine_id", "desc");
                                $this->db->where('day' , $day);
                                $this->db->where('class_id' , $class_id);
                                $this->db->where('section_id' , $row['section_id']);
								
                                $this->db->where('year' , $running_year);
                                $routines   =   $this->db->get('class_routine')->result_array();
								
								
                                foreach($routines as $row2):
                                ?><tr>
									<td width="30%" style="vertical-align:middle;"><?php echo $this->crud_model->get_subject_name_by_id($row2['subject_id']);?></td>
                                    <td class="clase_text" style="vertical-align:middle;" width="10% "><?php echo ($day);?></td>
                                    <td class="clase_text" style="vertical-align:middle;" width="10%"><?php echo ($row2['date']);?></td>
                                    <td class="clase_text"style="vertical-align:middle;" width="10%">
                              <!--   <div class="btn-group"> -->
                                    
                                    <!-- <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> -->
                                        
                                        <?php
                                        $changetime = 12;
                                            if ($row2['time_start_min'] == 0 && $row2['time_end_min'] == 0){
                                                echo $row2['time_start'].' : 00 AM<br>';
                                            echo $row2['time_end']-$changetime.' : 00 PM';
                                            }
                                            else{
                                         echo $row2['time_start'].':'.$row2['time_start_min'].' AM<br>'; 
                                          echo $row2['time_end']-$changetime.':'.$row2['time_end_min'].' PM';
                                        } ?>
                                        </td>
                                        <td  class="clase_text"style="vertical-align:middle;" width="22%">
<?php echo ($row2['place']);?>                                      
                                        
                                        
                                        
                                        </td>
                                            
                                                <td    width="30%" style="vertical-align:middle;">
                                        <!-- <span class="caret"></span>
                                    </button> -->
                                    <!-- <ul class="dropdown-menu">
                                        <li> -->
                                        <a href="#" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/modal_edit_class_routine/<?php echo $row2['class_routine_id'];?>');">
                                            <i class="entypo-pencil"></i>
                                                <?php echo get_phrase('edit');?>
                                                        </a>
                                 <!-- </li>
                                 
                                 <li> -->
                                    <a href="#" onclick="confirm_modal('<?php echo base_url();?>index.php?admin/class_routine/delete/<?php echo $row2['class_routine_id'];?>');">
                                        <i class="entypo-trash"></i>
                                            <?php echo get_phrase('delete');?>
                                        </a>
                                        </td></tr>
                                <!--     </li>
                                    </ul>
                                </div> -->
                                
                                <?php endforeach;?>

                            </td>
                        </tr>
                        <?php endfor;?>
                        
                    </tbody>
                </table>
                
            </div>
        </div>

    </div>

</div>
<?php endforeach;?>
<?php endif;?>

</div>
