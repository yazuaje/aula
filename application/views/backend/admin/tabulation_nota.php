		
  <a class="bt_atras" href="index.php?admin">Volver al Inicio</a>

<style>
.sidebar-menu {
    display: none !important;
}
td{
	padding: 0px!important;
	vertical-align: middle !important;
	min-height: 24px !important;
}
td input{
	height: 30px;
	border: 0px;
}
td input:focus{
	background-color: #fff9c4;
}
</style>

<hr />
<div class="row">
	<div class="col-md-12">
		<?php echo form_open(base_url() . 'index.php?admin/tabulation_nota');?>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label"><?php echo get_phrase('class');?></label>
					<select name="class_id" class="form-control selectboxit" onchange="select_section(this.value)">
                        <option value=""><?php echo get_phrase('select_a_class');?></option>
                        <?php
                        $classes = $this->db->get('class')->result_array();
                        foreach($classes as $row):
                        	print_r($classes);
                        ?>
                            <option value="<?php echo $row['class_id'];?>"
                            	<?php if ($class_id == $row['class_id']) echo 'selected';?>>
                            		<?php echo $row['name'];?>
                            </option>
                        <?php
                        endforeach;
                        ?>
                    </select>
				</div>
			</div>
			<div class="col-md-4">
				<div id="section_holder">
					<div class="col-md-12">
				<div class="form-group">
					<label  style="margin-bottom: 5px;" class="control-label"><?php echo get_phrase('section');?></label>
					<select class="form-control selectboxit" name="section_id">
                            <option value=""><?php echo get_phrase('select_class_first') ?></option>

			</select>
				</div>
			</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<label class="control-label"><?php echo get_phrase('exam');?></label>
					<select name="exam_id" class="form-control selectboxit">
                        <option value=""><?php echo get_phrase('select_an_exam');?></option>
                        <?php
                        $exams = $this->db->get_where('exam' , array('year' => $running_year))->result_array();
                        foreach($exams as $row):
                        ?>
                            <option value="<?php echo $row['exam_id'];?>"
                            	<?php if ($exam_id == $row['exam_id']) echo 'selected';?>>
                            		<?php echo $row['name'];?>
                            </option>
                        <?php
                        endforeach;
                        ?>
                    </select>
				</div>
			</div>

			<input type="hidden" name="operation" value="selection">
			<div class="col-md-4" style="margin-top: 20px;">
				<button type="submit" class="btn btn-info"><?php echo get_phrase('view_tabulation_nota');?></button>
			</div>
		<?php echo form_close();?>
	</div>
</div>

<script type="text/javascript">
    function select_section(class_id) {

        $.ajax({
            url: '<?php echo base_url(); ?>index.php?admin/get_section2/' + class_id,
            success:function (response)
            {

                jQuery('#section_holder').html(response);
            }
        });
    }
</script>

<?php if ($class_id != '' && $section_id != '' && $exam_id != ''):?>
<br>
<div class="row">
	<div class="col-md-4"></div>
	<div class="col-md-4" style="text-align: center;">
		<div class="tile-stats tile-gray">
		<div class="icon"><i class="entypo-docs"></i></div>
			<h3 style="color: #696969;">
				<?php
					$suma_total=0;
					$exam_name  = $this->db->get_where('exam' , array('exam_id' => $exam_id))->row()->name;
					$section_name = $this->db->get_where('section' , array('section_id' => $section_id))->row()->name;
					$class_name = $this->db->get_where('class' , array('class_id' => $class_id))->row()->name;
					
				?>
			</h3>
			<h4 style="color: black;">
				<?php echo get_phrase('class') . ' ' . $class_name;?>  
				<br><?php echo $section_name;?>
				<br>

				<?php 
				?>
			</h4>
		</div>
		<form id="form_xlf" method="post">
		   	<input type="file" name="xlfile" id="xlf" style="display:none;" /> 
		   	<button 
		   		class="boton btn btn-info" 
		   		type="button" 
		   		name="enviar" 
		   		
		   		onclick="$('#xlf').trigger('click')" 
		   	/>
		   	Cargar Notas Por Excel
		   	</button>
		   	<button type="button" class="btn btn-danger" onclick="pdf();">Exportar PDF</button>
		   	<button type="button" class="btn btn-success" onclick="tableToExcel();">Exportar EXCEL</button>
		</form>
		
		<br>
		<?php echo form_open(base_url() . 'index.php?admin/exportar_pdf');?>
		<form id="formularioPDF" method="post">
		<input name="exam_id" value="<?php echo $exam_id;?>" type="hidden"></input>
		<input name="class_id" value="<?php echo $class_id;?>" type="hidden"></input>
		<input name="section_id" value="<?php echo $section_id;?>" type="hidden"></input>
		
		<?php echo form_close();?>
	</div>
	<div class="col-md-4"></div>
</div>


<hr />

<div class="row">
			<?php
					$teacher = $this->db->get_where('section' , array('class_id' => $class_id , 'section_id' => $section_id))->row()->teacher_id;
					$subjects = $this->db->get_where('subject' , array('class_id' => $class_id , 'teacher_id' => $teacher))->result_array();
					$enrolls = $this->db->get_where('enroll' , array('class_id' => $class_id , 'section_id' => $section_id))->result_array();
					$notas = $this->db->get_where('enroll' , array('class_id' => $class_id , 'section_id' => $section_id))->result_array();

			?>
	<div class="col-md-12">
		<?php echo form_open(base_url() . 'index.php?Admin/guardar_registro_nota/'.$exam_id.'/'.$class_id.'/'.$section_id.'/'.$subject_id);?>
		<table class="table table-bordered" id="tabla_alumnos">
			<thead>
				
				<tr>
					<th style="background-color:#003471;" class="text-center">DNI</th>
						<th style="background-color:#003471;" class="text-center">
							<?php echo get_phrase('apellidos_y_nombres');?>	
						</th>
						<?php $i=1; foreach($subjects as $row): ?>
							<th style="background-color:#003471;" class="text-center">
								<?php echo $row['nick_modulo'];?>								
							</th>
							<input type="hidden" name="<?php echo $row['nick_modulo'];?>" value= "<?php echo $row['nick_modulo'];?>">
							<input name="subjects_id[]" value="<?php echo $row["subject_id"];?>" type="hidden"></input>
						<?php $i++; endforeach; ?>
					
						<th style="background-color:#003471;" class="text-center">
						<?php echo get_phrase('Promedio');?>
							
					</th>
				</tr>
			</thead>
			<tbody>
			<?php
				foreach($enrolls as $row):
			$dni = $this->db->get_where('student' , array('student_id' => $row['student_id']))->row()->dni;
//echo '<pre>';print_r($row);
			?>
				<tr id="<?php echo $dni;?>" class="fila-registro">
                <td class="text-center">
						<?php 
						 echo $dni;?>
						 <input name="dni[]" value= "<?php echo $dni;?>" type="hidden"></input>
						  <input name="student_id[]" value= "<?php echo  $row['student_id'];?>" type="hidden"></input>
					</td>
					<td class="text-center">
						<?php $apellidos_y_nombres = $this->db->get_where('student' , array('student_id' => $row['student_id']))->row()->name; 
						echo $apellidos_y_nombres; ?>
					</td>
				<?php
					$total_marks = 0;
					$total_grade_point = 0;
					$total=0;
					foreach($subjects as $id => $row2):
						//echo '<pre>';print_r($row2);

				?>
					<td align="middler" style=" padding:0px !important;" class="modulo text-center">
						<input 
							min="0"
							max="100"
							validated
							validate
							required
							class="nota text-center"
							style="margin: 0px !important; width: 100%;" 
							type="number" 
							name="nota[<?php echo $row['student_id'];?>][<?php echo $subjects[$id]["subject_id"];?>]" 
							value="<?php echo $this->db->get_where('mark' , array('student_id' => $row['student_id'],'class_id' => $class_id , 'section_id' => $section_id, 'subject_id' =>$subjects[$id]["subject_id"]))->row()->mark_obtained;?>">
						</input>
						<div id="html_nota" 
							class="text-center"
						style="position: absolute; top:1px; z-index:-1;"><?php echo $this->db->get_where('mark' , array('student_id' => $row['student_id'],'class_id' => $class_id , 'section_id' => $section_id, 'subject_id' =>$subjects[$id]["subject_id"]))->row()->mark_obtained;?></div>

					</td>
				<?php endforeach;
			 
				?>
				
				
				<td style="text-align: center;" class="promedio">
					<?php
						$promedio=$total_marks / count($subjects);
						echo number_format($promedio,2,".","");
						if($promedio >10){ $suma_total=$suma_total+1;}					
?>
				</td>
				</tr>

			<?php 		 endforeach; 	?>
<tr>
				<td></td>


		</table>

		<center>
			<button type="submit" class="btn btn-success" id="submit_button">
				<i class="entypo-check"></i> <?php echo get_phrase('save_changes');?>
			</button>
			
			</a>
		</center>
		<?php echo form_close();?>
	</div>
</div>
<script type="text/javascript">


	var pdf = function()
	{

		  var doc = new jsPDF({orientation:'l'});
  var specialElementHandlers = {
      '#editor': function (element, renderer) {
          return true;
      }
  };

  var elem = document.getElementById('tabla_alumnos');
  var data = doc.autoTableHtmlToJson(elem);
  doc.autoTable(data.columns, data.rows, {
         theme : 'grid',
        styles: {
               halign: 'center',
                },
        headerStyles: {
                     fillColor: [22, 44, 100],
                    halign:'center'
        },
        margin : {
        top : 10
        },
        columnStyles:{
        halign:'center'
        }
  });



  doc.save('tabled.pdf')

	}
	 var tableToExcel = (
        function() {
        var uri = 'data:application/vnd.ms-excel;base64,',
        template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" ><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        base64 = function(s) { 
            return window.btoa(unescape(encodeURIComponent(s))) 
        },
        format = function(s, c) { 
            return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) 
        }
        return function(name) {
            
            var table = document.getElementById('tabla_alumnos').innerHTML 
            
            if($('#tabla_total').length==0)
            {
                total = ''
            }else{
                total = total.innerHTML
            }
            var ctx = {worksheet: 'descarga.xml', table}
            window.location.href = uri + base64(format(template, ctx)) 
        }
    })()

	$(document).ready(function(){
		$('.fila-registro').each(function(i, f){
			var notaAcumulada = 0;
			var mod = $(f).find('.modulo');
			$.each(mod,function(x,m){
				var nota = parseFloat($(m).find('input').val())
				notaAcumulada+=nota;
			})
			var promedio = Math.round(notaAcumulada/mod.length);
			$(f).find('.promedio').html(promedio);
		})
		$('.nota').on('focus',function(){
			var nota = parseFloat($(this).val())
			if(nota == 0 ){
				$(this).val('')
				$(this).parent().find('div').html('')
			}
		})
		
		$('.nota').on('blur',function(){
			var nota = parseFloat($(this).val()).toFixed(1)
			if(nota == 0 || isNaN(nota) ){
				$(this).val(0)
				$(this).parent().find('div').html('0')
			}else{
				$(this).val(nota)	
				$(this).parent().find('div').html(nota)
			}
		})
		$('.nota').on('change',function(){
			var nota = parseFloat($(this).val())
			if(isNaN(nota)){
				$(this).val(0)
				$(this).parent().find('div').html('0')
			}else{
				$(this).val(nota.toFixed(1))
				$(this).parent().find('div').html(nota.toFixed(1))
			}
				
			var notaAcumulada = 0;
			var mod = $(this).parent().closest('tr').find('.modulo');
			$.each(mod,function(x,m){
				var nota = parseFloat($(m).find('input').val())
				notaAcumulada+=nota;
			})
			var promedio = Math.round(notaAcumulada/mod.length);
			$(this).parent().closest('tr').find('.promedio').html(promedio);
		})
		$("#frmArchivo").submit(function(){
			var datos = new FormData();
			datos.append('archivo',$('#archivo')[0].files[0]);
			$.ajax({
		    type:"post",
		    dataType:"json",
		    url:"importar.php",
		    contentType:false,
		    data:datos,
		    processData:false,
		  }).done(function(respuesta){
		    alert(respuesta.mensaje);
			});
			  return false;
		});
	});	


	
</script>
<script src="<?=base_url()?>assets/js/jsPDF-master/dist/jspdf.debug.js"></script>
<script src="<?=base_url()?>assets/js/jsPDF-master/dist/jspdf.min.js"></script>
<script src="<?=base_url()?>assets/js/jsPDF-AutoTable-master/dist/jspdf.plugin.autotable.min.js"></script>
<script src="<?=base_url()?>assets/js/cpexcel.js"></script>
<script src="<?=base_url()?>assets/js/xlsx.js"></script>
<script src="<?=base_url()?>assets/js/cargarNotasExcel.js"></script>
<?php endif;?>
