<?php

if (!defined('BASEPATH'))

    exit('No direct script access allowed');



/*

 *	@author : Joyonto Roy

 *	date	: 20 August, 2013

 *	University Of Dhaka, Bangladesh

 *	Ekattor School & College Management System

 *	http://codecanyon.net/user/joyontaroy

 */



class Student extends CI_Controller

{





    function __construct()

    {

        parent::__construct();

		$this->load->database();

        $this->load->library('session');

        /*cache control*/

        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');

        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');

        $this->output->set_header('Pragma: no-cache');

        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");

    }



    /***default functin, redirects to login page if no admin logged in yet***/

    public function index()

    {

        if ($this->session->userdata('student_login') != 1)

            redirect(base_url() . 'index.php?login', 'refresh');

        if ($this->session->userdata('student_login') == 1)

            redirect(base_url() . 'index.php?student/dashboard', 'refresh');

    }

    function principal(){

        $page_data = array();
        
        
        $userId = $this->session->userdata('student_id');
        $courses =  $this->db->query('select s.subject_id,.c.class_id,c.name,s.name as sub_name  from enroll e, class c,subject s,teacher t where e.student_id = '.$userId.' and  c.class_id = s.class_id  and e.class_id = c.class_id group by class_id');
                    
        $page_data['courses']  = $courses->result_array(); 


        $page_data['page_name']  = 'principal';
        $this->load->view('backend/index', $page_data);
    }

    /***ADMIN DASHBOARD***/

    function dashboard($param)

    {   
        //verifcamos la clase 
       

        $page_data['class_id'] = $param;
        if ($this->session->userdata('student_login') != 1)

            redirect(base_url(), 'refresh');



$student_profile = $this->db->get_where('student', array(

            'student_id' => $this->session->userdata('student_id')

            ))->row();



 $sql = '';
            if(isset($param2))    
                $sql = "'class_id' => $param2";

            $page_data['class_id'] = $this->db->get_where('enroll' , array(

            
            'student_id' => $student_profile->student_id,
            'class_id' => $param,

                'year' => $this->db->get_where('settings' , array('type' => 'running_year'))->row()->description,
            
               

            ))->row()->class_id;



            $page_data['section_id'] = $this->db->get_where('enroll' , array(

            'student_id' => $student_profile->student_id,

                'class_id' => $page_data['class_id'],

                    'year' => $this->db->get_where('settings' , array('type' => 'running_year'))->row()->description

            ))->row()->section_id;





        $page_data['page_name']  = 'dashboard';

        $page_data['page_title'] = get_phrase('student_dashboard');

        $this->load->view('backend/index', $page_data);
    }



    public function clases(){
        $page_data = array();
        $page_data['page_name']  = 'clases';
        $page_data['page_title'] = get_phrase('cursos_disponibles');
        $this->load->view('backend/index', $page_data);




    }





    /****MANAGE TEACHERS*****/

   function teacher_list($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('student_login') != 1)
            redirect(base_url(), 'refresh');
			
        if ($param1 == 'personal_profile') {
            $page_data['personal_profile']   = true;
            $page_data['current_teacher_id'] = $param2;
        }
        $page_data['teachers']   = $this->db->get('teacher')->result_array();
        $page_data['page_name']  = 'teacher';
        $page_data['page_title'] = get_phrase('NUESTROS DOCENTES');
        $this->load->view('backend/index', $page_data);
    }
   /* function teacher_list($param1 = '', $param2 = '', $param3 = '')

    {
        $page_data = array();
        if ($this->session->userdata('student_login') != 1)

            redirect(base_url(), 'refresh');

        $page_data['teachers']   = $this->db->get('teacher')->result_array();
        if ($param1 == 'personal_profile') {
  

            $page_data['personal_profile']   = true;

            $page_data['current_teacher_id'] = $param2;

            $teachers =  $this->db->query('select t.teacher_id,t.name from teacher t, class c where c.teacher_id = t.teacher_id and c.class_id = '.$param2);
                    
            $page_data['teachers']  = $teachers->result_array(); 
            
            

        }

        

        $page_data['page_name']  = 'teacher';

        $page_data['page_title'] = get_phrase('manage_teacher');

        $this->load->view('backend/index', $page_data);

    }
*/







    /***********************************************************************************************************/







    /****MANAGE SUBJECTS*****/

    function subject($param1 = '', $param2 = '')

    {

        if ($this->session->userdata('student_login') != 1)

            redirect(base_url(), 'refresh');



        $student_profile         = $this->db->get_where('student', array(

            'student_id' => $this->session->userdata('student_id')

        ))->row();

        $student_class_id        = $this->db->get_where('enroll' , array(

            'student_id' => $student_profile->student_id,

                'year' => $this->db->get_where('settings' , array('type' => 'running_year'))->row()->description

        ))->row()->class_id;
        if(isset($param1))
            $student_class_id = $param1;

        $page_data['subjects']   = $this->db->get_where('subject', array(

            'class_id' => $student_class_id,

                'year' => $this->db->get_where('settings' , array('type' => 'running_year'))->row()->description

        ))->result_array();

        $page_data['page_name']  = 'subject';

        $page_data['page_title'] = get_phrase('MODULOS');

        $this->load->view('backend/index', $page_data);

    }







    function student_marksheet($student_id = '',$param2) {

        if ($this->session->userdata('student_login') != 1)

            redirect('login', 'refresh');

        $sql = '';
        if(isset($param2))    
            $sql = ",'class_id' => $param2";


        $class_id     = $this->db->get_where('enroll' , array(

            'student_id' => $student_id , 'year' => $this->db->get_where('settings' , array('type' => 'running_year'))->row()->description,
            'class_id' => $param2

        ))->row()->class_id;

        $student_name = $this->db->get_where('student' , array('student_id' => $student_id))->row()->name;

        $class_name   = $this->db->get_where('class' , array('class_id' => $class_id))->row()->name;

        $page_data['page_name']  = 'student_marksheet';

        $page_data['page_title'] = get_phrase('CALIFICACIONES: ') . ' ' . $student_name . ' (' . get_phrase('class') . ' ' . $class_name . ')';

        $page_data['student_id'] = $student_id;

        $page_data['class_id']   = $class_id;

        $this->load->view('backend/index', $page_data);

    }

     function delete_file_mark($param1 = '', $param2 = null , $param3 = '') {

            if($param2 != null)

            {

                $data['file_name']="";

                unlink("uploads/document/" .  $param2);



            $this->db->where('mark_id', $param1);

            $this->db->update('mark', $data);

            }





            redirect(base_url() . 'ok_enviado.php', 'refresh');

     }



    function subir_trabajos($param1 = '') {


        //array de campos file del formulario

        $subjects = $_POST['subject']['name'];

        $marks = $_POST['mark']['name'];

         $files = $_FILES['userfile']['name'];

        //creamos una nueva instancia de la clase multiupload

        $i = 0;

        $temp=null;



        foreach($files as $file)

        {



                    //var_dump($_FILES['userfile']['tmp_name'][$i]);

            //si se está subiendo algún archivo en ese indice

            if($_FILES['userfile']['tmp_name'][$i]==null || $_FILES['userfile']['tmp_name'][$i]=="")

            {

              // var_dump(' if '.($temp.'/'.$_POST['mark'][$i]));

                 if($temp==$_POST['mark'][$i]){

                    //var_dump('No ha sido creado el Mark para esta calificacion');

                            //var_dump($files);



                }

            $temp=$_POST['mark'][$i];



            }

            else{

               // $string = $string.' else '.($temp.'/'.$_POST['mark'][$i]);

               // var_dump(' else out '.($temp.'/'.$_POST['mark'][$i]));



                if($temp!=$_POST['mark'][$i]){

                //var_dump(' else '.($temp.'/'.$_POST['mark'][$i]));



                 //var_dump($_POST['mark'][$i]);

                 //var_dump($_FILES['userfile']['name'][$i]);

                 $data['file_name'] = $_FILES['userfile']['name'][$i];

                 //var_dump($data['file_name']);

                 $this->db->where('mark_id', $_POST['mark'][$i] );

                 $this->db->update('mark', $data);

                 if(move_uploaded_file($_FILES['userfile']['tmp_name'][$i],"uploads/document/".$_FILES['userfile']['name'][$i]))

                    {

$datos=$this->db->get_where('mark',array('mark_id', $_POST['mark'][$i]))->row();
print_r($datos);
/*
 $nombres = $this->db->get_where('student' , array('student_id' => $student_id))->row()->name;
 $mail = $this->db->get_where('student' , array('student_id' => $student_id))->row()->email;
		$mensaje_html='<table style="width:100% ;"><tr><td><table style="background:#005aa6;width:100%">
<tr><td><img src="'.base_url().'/aula/uploads/logo.png"></td><td></td></tr>
</table></td></tr><tr>
<td colspan="2" style="border-bottom:2px solid #005aa6"><h3>Estimado (a):'.$nombres.'</h3></td>
</tr><tr>
<td colspan="2" style="text-align:justify;padding:15px"> Su Archivo fue subido correctamente en el sistema.</td>
</tr><tr><td colspan="2"><table style="width:70%" align="center">
<tr><td><h3><u>Datos de la Clase</u></h3> </td><td rowspan="3"> </td></tr>
<tr><td>Clases '.$clase.'</td></tr>
<tr><td>Archvio <a style="background:#005aa6;color:white;padding:5px;font-weight:bold;text-decoration:none" href="">Ingresar al Aula Virtual</a></td></tr></table></td><tr>
<tr><td colspan="2"  align="center"><a style="background:#005aa6;color:white;padding:5px;font-weight:bold;text-decoration:none" href="http://www.nuevohorizonte.edu.pe/aula">Ingresar al Aula Virtual</a></td></tr></table>';
 $this->email_model->do_email_all($mensaje_html, 'Archivo Subido Correctamente', $mails,'hluis70@yahoo.com');
*/

                       // echo "subida correctamente";

                        //aqui podemos procesar info de la bd referente a este archivo

                    }

                    else{

                        echo "<h2>Error al subir, por favor intente mas tarde o comunique a soporte tecnico</h2>";

                    }

            redirect(base_url() . 'ok_enviado.php'.$param3, 'refresh');


                }





            }



        $i++;

       // var_dump($temp);

    }

    //var_dump($string);

  //     redirect(base_url() . 'index.php?student/student_marksheet/'.$param1, 'refresh');

}





    function student_marksheet_print_view($student_id , $exam_id) {

        if ($this->session->userdata('student_login') != 1)

            redirect('login', 'refresh');

        $class_id     = $this->db->get_where('enroll' , array(

            'student_id' => $student_id , 'year' => $this->db->get_where('settings' , array('type' => 'running_year'))->row()->description

        ))->row()->class_id;

        $class_name   = $this->db->get_where('class' , array('class_id' => $class_id))->row()->name;



        $page_data['student_id'] =   $student_id;

        $page_data['class_id']   =   $class_id;

        $page_data['exam_id']    =   $exam_id;

        $this->load->view('backend/student/student_marksheet_print_view', $page_data);

    }





    /**********MANAGING CLASS ROUTINE******************/

    function class_routine($param1 = '', $param2 = '', $param3 = '')

    {

        $sql = '';
        if ($this->session->userdata('student_login') != 1)

            redirect(base_url(), 'refresh');

        $sub_sql = isset($param1) ? "'class_id' => $param1" : '';

        $student_profile         = $this->db->get_where('student', array(

            'student_id' => $this->session->userdata('student_id'),

        ))->row();

        $page_data['class_id']   = $this->db->get_where('enroll' , array(

            'student_id' => $student_profile->student_id,
            'class_id' => $param1,

                'year' => $this->db->get_where('settings' , array('type' => 'running_year'))->row()->description

        ))->row()->class_id;

        $page_data['student_id'] = $student_profile->student_id;

        $page_data['page_name']  = 'class_routine';

        $page_data['page_title'] = get_phrase('CRONOGRAMA DE CLASES');

        $this->load->view('backend/index', $page_data);

    }



    function class_routine_print_view($class_id , $section_id)

    {

        if ($this->session->userdata('student_login') != 1)

            redirect('login', 'refresh');

        $page_data['class_id']   =   $class_id;

        $page_data['section_id'] =   $section_id;

        $this->load->view('backend/admin/class_routine_print_view' , $page_data);

    }



    // ACADEMIC SYLLABUS

    function academic_syllabus($student_id = '')

    {

        if ($this->session->userdata('student_login') != 1)

            redirect(base_url(), 'refresh');



        $page_data['page_name']  = 'academic_syllabus';

        $page_data['page_title'] = get_phrase('academic_syllabus');

        $page_data['student_id']   = $student_id;

        $this->load->view('backend/index', $page_data);

    }



    function download_academic_syllabus($academic_syllabus_code)

    {

        $file_name = $this->db->get_where('academic_syllabus', array(

            'academic_syllabus_code' => $academic_syllabus_code

        ))->row()->file_name;

        $this->load->helper('download');

        $data = file_get_contents("uploads/syllabus/" . $file_name);

        $name = $file_name;



        force_download($name, $data);

    }



    /******MANAGE BILLING / INVOICES WITH STATUS*****/

    function invoice($param1 = '', $param2 = '', $param3 = '')

    {

        //if($this->session->userdata('student_login')!=1)redirect(base_url() , 'refresh');

        if ($param1 == 'make_payment') {

            $invoice_id      = $this->input->post('invoice_id');

            $system_settings = $this->db->get_where('settings', array(

                'type' => 'paypal_email'

            ))->row();

            $invoice_details = $this->db->get_where('invoice', array(

                'invoice_id' => $invoice_id

            ))->row();



            /****TRANSFERRING USER TO PAYPAL TERMINAL****/

            $this->paypal->add_field('rm', 2);

            $this->paypal->add_field('no_note', 0);

            $this->paypal->add_field('item_name', $invoice_details->title);

            $this->paypal->add_field('amount', $invoice_details->amount);

            $this->paypal->add_field('custom', $invoice_details->invoice_id);

            $this->paypal->add_field('business', $system_settings->description);

            $this->paypal->add_field('notify_url', base_url() . 'index.php?student/invoice/paypal_ipn');

            $this->paypal->add_field('cancel_return', base_url() . 'index.php?student/invoice/paypal_cancel');

            $this->paypal->add_field('return', base_url() . 'index.php?student/invoice/paypal_success');



            $this->paypal->submit_paypal_post();

            // submit the fields to paypal

        }

        if ($param1 == 'paypal_ipn') {

            if ($this->paypal->validate_ipn() == true) {

                $ipn_response = '';

                foreach ($_POST as $key => $value) {

                    $value = urlencode(stripslashes($value));

                    $ipn_response .= "\n$key=$value";

                }

                $data['payment_details']   = $ipn_response;

                $data['payment_timestamp'] = strtotime(date("m/d/Y"));

                $data['payment_method']    = 'paypal';

                $data['status']            = 'paid';

                $invoice_id                = $_POST['custom'];

                $this->db->where('invoice_id', $invoice_id);

                $this->db->update('invoice', $data);



                $data2['method']       =   'paypal';

                $data2['invoice_id']   =   $_POST['custom'];

                $data2['timestamp']    =   strtotime(date("m/d/Y"));

                $data2['payment_type'] =   'income';

                $data2['title']        =   $this->db->get_where('invoice' , array('invoice_id' => $data2['invoice_id']))->row()->title;

                $data2['description']  =   $this->db->get_where('invoice' , array('invoice_id' => $data2['invoice_id']))->row()->description;

                $data2['student_id']   =   $this->db->get_where('invoice' , array('invoice_id' => $data2['invoice_id']))->row()->student_id;

                $data2['amount']       =   $this->db->get_where('invoice' , array('invoice_id' => $data2['invoice_id']))->row()->amount;

                $this->db->insert('payment' , $data2);

            }

        }

        if ($param1 == 'paypal_cancel') {

            $this->session->set_flashdata('flash_message', get_phrase('payment_cancelled'));

            redirect(base_url() . 'index.php?student/invoice/', 'refresh');

        }

        if ($param1 == 'paypal_success') {

            $this->session->set_flashdata('flash_message', get_phrase('payment_successfull'));

            redirect(base_url() . 'index.php?student/invoice/', 'refresh');

        }

        $student_profile         = $this->db->get_where('student', array(

            'student_id'   => $this->session->userdata('student_id')

        ))->row();

        $student_id              = $student_profile->student_id;

        $page_data['invoices']   = $this->db->get_where('invoice', array(

            'student_id' => $student_id

        ))->result_array();

        $page_data['page_name']  = 'invoice';

        $page_data['page_title'] = get_phrase('manage_invoice/payment');

        $this->load->view('backend/index', $page_data);

    }



    /**********MANAGE LIBRARY / BOOKS********************/

    function book($param1 = '', $param2 = '', $param3 = '')

    {

        if ($this->session->userdata('student_login') != 1)

            redirect('login', 'refresh');



        $page_data['books']      = $this->db->get('book')->result_array();

        $page_data['page_name']  = 'book';

        $page_data['page_title'] = get_phrase('manage_library_books');

        $this->load->view('backend/index', $page_data);



    }

    /**********MANAGE TRANSPORT / VEHICLES / ROUTES********************/

    function transport($param1 = '', $param2 = '', $param3 = '')

    {

        if ($this->session->userdata('student_login') != 1)

            redirect('login', 'refresh');



        $page_data['transports'] = $this->db->get('transport')->result_array();

        $page_data['page_name']  = 'transport';

        $page_data['page_title'] = get_phrase('manage_transport');

        $this->load->view('backend/index', $page_data);



    }

    /**********MANAGE DORMITORY / HOSTELS / ROOMS ********************/

    function dormitory($param1 = '', $param2 = '', $param3 = '')

    {

        if ($this->session->userdata('student_login') != 1)

            redirect('login', 'refresh');



        $page_data['dormitories'] = $this->db->get('dormitory')->result_array();

        $page_data['page_name']   = 'dormitory';

        $page_data['page_title']  = get_phrase('manage_dormitory');

        $this->load->view('backend/index', $page_data);



    }



    /**********WATCH NOTICEBOARD AND EVENT ********************/

    function noticeboard($param1 = '', $param2 = '', $param3 = '')

    {

        if ($this->session->userdata('student_login') != 1)

            redirect('login', 'refresh');



        $page_data['notices']    = $this->db->get('noticeboard')->result_array();

        $page_data['page_name']  = 'noticeboard';

        $page_data['page_title'] = get_phrase('ANUNCIOS INSTITUCIONALES');

        $this->load->view('backend/index', $page_data);



    }



    /**********MANAGE DOCUMENT / home work FOR A SPECIFIC CLASS or ALL*******************/

    function document($do = '', $document_id = '')

    {

        if ($this->session->userdata('student_login') != 1)

            redirect('login', 'refresh');



        $page_data['page_name']  = 'manage_document';

        $page_data['page_title'] = get_phrase('manage_documents');

        $page_data['documents']  = $this->db->get('document')->result_array();

        $this->load->view('backend/index', $page_data);

    }



    /* private messaging */



    function message($param1 = 'message_home', $param2 = '', $param3 = '') {

        if ($this->session->userdata('student_login') != 1)

            redirect(base_url(), 'refresh');



        if ($param1 == 'send_new') {

            $message_thread_code = $this->crud_model->send_new_private_message();

            $this->session->set_flashdata('flash_message', get_phrase('message_sent!'));

            redirect(base_url() . 'index.php?student/message/message_read/' . $message_thread_code, 'refresh');

        }



        if ($param1 == 'send_reply') {

            $this->crud_model->send_reply_message($param2);  //$param2 = message_thread_code

            $this->session->set_flashdata('flash_message', get_phrase('message_sent!'));

            redirect(base_url() . 'index.php?student/message/message_read/' . $param2, 'refresh');

        }



        if ($param1 == 'message_read') {

            $page_data['current_message_thread_code'] = $param2;  // $param2 = message_thread_code

            $this->crud_model->mark_thread_messages_read($param2);

        }



        $page_data['message_inner_page_name']   = $param1;

        $page_data['page_name']                 = 'message';

        $page_data['page_title']                = get_phrase('MENSAJES');

        $this->load->view('backend/index', $page_data);

    }



    /******MANAGE OWN PROFILE AND CHANGE PASSWORD***/

    function manage_profile($param1 = '', $param2 = '', $param3 = '')

    {

        if ($this->session->userdata('student_login') != 1)

            redirect(base_url() . 'index.php?login', 'refresh');

        if ($param1 == 'update_profile_info') {

            $data['name']     = $this->input->post('name');

            $data['email']    = $this->input->post('email');

            $data['phone']    = $this->input->post('phone');

            $data['address']  = $this->input->post('address');

            $data['birthday'] = $this->input->post('birthday');

            $data['sex']      = $this->input->post('sex');



            $this->db->where('student_id', $this->session->userdata('student_id'));

            $this->db->update('student', $data);

            move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/student_image/' . $this->session->userdata('student_id') . '.jpg');

            $this->session->set_flashdata('flash_message', get_phrase('account_updated'));

            redirect(base_url() . 'index.php?student/manage_profile/', 'refresh');

        }

        if ($param1 == 'change_password') {

            $data['password']             = sha1($this->input->post('password'));

            $data['new_password']         = sha1($this->input->post('new_password'));

            $data['confirm_new_password'] = sha1($this->input->post('confirm_new_password'));



            $current_password = $this->db->get_where('student', array(

                'student_id' => $this->session->userdata('student_id')

            ))->row()->password;

            if ($current_password == $data['password'] && $data['new_password'] == $data['confirm_new_password']) {

                $this->db->where('student_id', $this->session->userdata('student_id'));

                $this->db->update('student', array(

                    'password' => $data['new_password']

                ));

                $this->session->set_flashdata('flash_message', get_phrase('password_updated'));

            } else {

                $this->session->set_flashdata('flash_message', get_phrase('password_mismatch'));

            }

            redirect(base_url() . 'index.php?student/manage_profile/', 'refresh');

        }

        $page_data['page_name']  = 'manage_profile';

        $page_data['page_title'] = get_phrase('CUENTA');

        $page_data['edit_data']  = $this->db->get_where('student', array(

            'student_id' => $this->session->userdata('student_id')

        ))->result_array();

        $this->load->view('backend/index', $page_data);

    }



    /*****************SHOW STUDY MATERIAL / for students of a specific class*******************/

    function study_material($param)

    {

        if ($this->session->userdata('student_login') != 1)

        {

            $this->session->set_userdata('last_page' , current_url());

            redirect(base_url(), 'refresh');

        }



        $data['study_material_info']    = $this->crud_model->select_study_material_info_for_student($param);

        $data['page_name']              = 'study_material';

        $data['page_title']             = get_phrase('REPOSITORIO ACADEMICO');

        $this->load->view('backend/index', $data);

    }



    function study_slide($param)

    {

        if ($this->session->userdata('student_login') != 1)

        {

            $this->session->set_userdata('last_page' , current_url());

            redirect(base_url(), 'refresh');

        }



        $data['study_material_info']    = $this->crud_model->select_diapositiva_for_student($param);

        $data['page_name']              = 'study_slide';

        $data['page_title']             = get_phrase('VIDEOS');

        $this->load->view('backend/index', $data);

    }



    function manage_attendance_report_student($id_student = '',$param) {

        if ($this->session->userdata('student_login') != 1)

            redirect(base_url(), 'refresh');


             $student_profile = $this->db->get_where('student', array(

            'student_id' => $this->session->userdata('student_id')

            ))->row();

            
            $sql = '';
            if(isset($param2))    
                $sql = "'class_id' => $param2";

            $page_data['class_id'] = $this->db->get_where('enroll' , array(

            
            'student_id' => $student_profile->student_id,
            'class_id' => $param,

                'year' => $this->db->get_where('settings' , array('type' => 'running_year'))->row()->description,
            
               

            ))->row()->class_id;



            $page_data['section_id'] = $this->db->get_where('enroll' , array(

            'student_id' => $student_profile->student_id,

                'class_id' => $page_data['class_id'],

                    'year' => $this->db->get_where('settings' , array('type' => 'running_year'))->row()->description

            ))->row()->section_id;



            $page_data['enrolls'] = $this->db->get_where('enroll' , array('class_id' => $page_data['class_id'] , 'section_id' => $page_data['section_id'], 'student_id' => $student_profile->student_id))->result_array();



        $page_data['student_id'] = $student_profile->student_id;

        $page_data['page_name']  =	'attendance_report_student';

        $page_data['page_title'] =	get_phrase('ASISTENCIA');

		$this->load->view('backend/index', $page_data);



    }

}
